# This is an extra layer on top of the json or json_simple gem to
# make it serialize and de-serialize graphs. The resulting json is
# ALMOST compatable with the Java json_io library
#    They need Java-style '.' separated instead of '::' separated package paths
#    They need key '@type' rather than 'json_class'

# This will work with either 
#    gem install json
# or 
#    gem install json_simple

require 'json' #  Do not need to  require 'json/add/core'; require 'json/add/rails'
require 'forwardable'
require 'singleton'
require 'common/ruby_extensions'
require 'json_graph/time_marshalling'

# This should not really be a singleton. It prevents the code from being thread safe.
#  You must not marshall two graphs simultaneously.
#  Good enough for the moment though.
class JSON_Graph_State
  include Singleton
  extend SingleForwardable
  def_delegators :instance, 
  # State
  :seen, :id_mapping, :previous_marshalling_id, 
  :clear, :marshalling_id_for, :ignore_getter?, :excludes_for, 
  :references?, :no_references!, :std_references!, :classname?, :no_classNames!,
  # Attribute naming convention
  :classname_key, :classname_key=, :id_key, :id_key=, :ref_key, :ref_key=,
  # Traversal configuration
  :ignored_getters, :ignored_getters=,
  :ignored_getters_by_class, :ignored_getters_by_class=,
  :getter_tests, :getter_tests=,
  :getter_tests_by_class, :getter_tests_by_class=,
  :json_getter_overrides_by_class, :json_getter_overrides_by_class=,
  :excludes_for, :excludes_for_class,
  :formatting_rules, :formatting_rules_by_class,
  :formatting_rules=, :formatting_rules_by_class=
  
  attr_accessor :seen, :id_mapping, :previous_marshalling_id
  
  # ==== These are policies rather than state, but not worth separating ====
  attr_accessor :classname_key # Name of the JSON attribute that holds the name of the class
  # These attributes are used to accomodate cycles in the object graph
  attr_accessor :id_key        # Name of the JSON attribute that holds the marshalling id (which may be referenced by :ref_key)
  attr_accessor :ref_key       # Name of the JSON attribute that holds a value corresponding to an id_key, in order to reference a previously encountered object
  
  # These contain information used to traverse and marshal information. 
  attr_accessor :ignored_getters # Array of getters to be ignored (may differ from one use case to another)
  attr_accessor :ignored_getters_by_class # { class => Array-of-getters-to-be-ignored } (may differ from one use case to another)
  attr_accessor :getter_tests # Array of 2-arg Procs.  They take object & getter, and return false if getter should not be included.
  attr_accessor :getter_tests_by_class  # { class => array-of-2-arg-Procs }
  attr_accessor :formatting_rules  # Array of 1-arg Procs.  They take a string, format it, and return it.
  attr_accessor :formatting_rules_by_class  # { class => array-of-1-arg-Procs }
  attr_accessor :json_getter_overrides_by_class # { class => Hash-of-getter-mappings } (used by default implementation of json_getter_overrides)
  
  def initialize
    self.seen = Hash.new         #  {marshlling_id => instance }
    self.id_mapping = Hash.new   #  {json_id => marshalling_id }
    self.classname_key = '@type' #  Java's json_io requires '@type'.  Ruby uses whatever is set by JSON.create_id= , which defaults to 'json_class'
    self.id_key = '@id'          #  If either id_key or ref_key is nil, references are not allowed
    self.ref_key = '@ref'        #  If either id_key or ref_key is nil, references are not allowed
    self.getter_tests = Array.new
    self.getter_tests_by_class = Hash.new
    self.formatting_rules = Array.new
    self.formatting_rules_by_class = Hash.new
    self.ignored_getters = Array.new
    self.ignored_getters_by_class = Hash.new
    self.json_getter_overrides_by_class = Hash.new
  end
  def clear
    seen.clear
    id_mapping.clear
    self.previous_marshalling_id = 0
  end
  def self.clear_config
    json_getter_overrides_by_class.clear
    ignored_getters.clear
    ignored_getters_by_class.clear
    getter_tests.clear
    getter_tests_by_class.clear
    std_references!
  end
  def marshalling_id_for(object)
    id_mapping[object.json_id] ||= self.previous_marshalling_id+=1
  end
  def run_test(proc, object, getter, hash)
    case proc.arity
      when 2, -2 ; return proc.call(object, getter)
      when 3, -3 ; return proc.call(object, getter, hash)
      else raise "Unexpected arity #{proc.arity} for object of class #{object.class}"
    end
  end
  def ignore_getter?(getter, object, hash)
    getter = getter.to_sym
debuggy=false # FIXME
    getter_test_ok = getter_tests.inject(true) do |truth_val, proc|
      next unless truth_val
      truth_val && run_test(proc, object, getter, hash)
    end
    # return true unless getter_test_ok
    gtbc = getter_tests_by_class[object.class]
    if gtbc
      getter_test_by_class_ok = gtbc.inject(true) do |truth_val, proc|
        next unless truth_val
        truth_val && run_test(proc, object, getter, hash)
      end
      getter_test_ok = getter_test_ok && getter_test_by_class_ok
      # return true unless getter_test_by_class_ok
    end
    if !getter_test_ok
      puts "     rejecting #{getter} for #{object.class} by test" if debuggy
      return true
    else
      puts "#{getter} for #{object.class} test OK" if debuggy
    end
    ig = ignored_getters.member?(getter)
    if ig
      puts "rejecting #{getter} by ignore list" if debuggy
      return true
    end
    # return true if ignored_getters.member?(getter)
    conditional = ignored_getters_by_class[object.class]
    conditional && conditional.member?(getter)
  end
  def references?; id_key && ref_key; end
  def no_references!; self.id_key = nil; self.ref_key=nil; end
  def std_references!; self.id_key = '@id'; self.ref_key='@ref'; end
  def no_classNames! 
    _ignored_getters = JSON_Graph_State.instance.ignored_getters
    _name_getter = JSON_Graph_State.classname_key
    _ignored_getters << _name_getter unless _ignored_getters.member?(_name_getter)
  end
  def classname?; !ignored_getters.member?(classname_key); end
  def excludes_for_class(klass)
    conditional = ignored_getters_by_class[klass]
    return ignored_getters unless conditional
    ignored_getters + conditional
  end
  def excludes_for(object)
    excludes_for_class(object.class)
  end
end


$JSON_EXCLUDES = {} #  { class_name => array_of_getters_to_exclude_for_specific_class }

class Object
  # Override if you want
  def json_output_clean; self; end
end

class String
  def json_output_clean; strip; end
  # Raise an error if duplicate keys are present
  def no_duplicate_json_ids!
    return unless JSON_Graph_State.id_key
    ids = scan(/"#{JSON_Graph_State.id_key}": "(.*)",/).collect {|groups| groups.first}
    uniq_ids = ids.uniq
    return if ids.size == uniq_ids.size
    dupes = ids.group_by {|e|e}.select {|k, v|v.size > 1}.keys
    raise "Invalid JSON, duplicated ids found: #{dupes}"
  end
  def to_java_pkg_path; gsub('::', '.'); end
  def to_ruby_pkg_path; gsub('.', '::'); end
  def string2obj(state=JSON_Graph_State.instance); JSON_Graph.string2obj(self, state); end
  def path2obj(state=JSON_Graph_State.instance); JSON_Graph.path2obj(self, state); end
end

# Use to_s_with_nsec for DateTime & Time
class DateTime
  def json_output_clean; to_s_with_nsec; end
end
class Time
  def json_output_clean; to_s_with_nsec; end
end
class Date; end

module JSON_Graph
  extend SingleForwardable
  def_delegators :recipient, :generate, :fast_generate, :pretty_generate, :dump, :parse, :parse!, :load
  
  # This is the recipient of messages delegated as specified by the above def_delegators.
  # We could delegate directly to JSON, but we want to first want to execute some setup code.
  def self.recipient
    JSON_Graph_State.clear
    JSON.create_id=nil # See comment in Instantiable#json_create
    JSON
  end
  
  # Parse a JSON string into ruby objects
  def self.string2obj(string, state=JSON_Graph_State.instance)
    parse(string).to_json_objs(state)
  end
  
  # Parse a JSON file into ruby objects
  def self.path2obj(path, state=JSON_Graph_State.instance)
    string = File.read(path)
    string2obj(string, state)
  end
  
  def self.omit_nil_attributes?
    @omit_nil_attributes
  end
  def self.omit_nil_attributes=(value)
    @omit_nil_attributes = value
  end
  
  # These methods are added to the class side of any class you mix (include) Serializeable into
  module Serializeable_Class_Side
    # This method defines the broadest set of possible json_getters for the class that has this method
    # These really are json getters (keys), not Ruby getters.  
    # Ruby getters are the same as json getters, unless json_getter_overrides is used to change the Ruby getter.
    # The phrase "broadest set" means that some of these getters may not be used for some purposes.
    # You can ignore json getters in a use-case specific way by using:
    #   * ignored_getters  (does not depend on specific instance)
    #   * ignored_getters_by_class (does not depend on specific instance)
    #   * getter_tests (may depend on specific instance)
    #   * getter_tests_by_class (may depend on specific instance)
    # Currently we treat getting and setting symmetrically (if you can get, then you can set).
    # We derive the ruby setter from the ruby getter by adding "="
    def json_getters
      ret = json_getter_overrides.keys
      # puts "JSON getters for #{self} are #{ret}"
      ret
    end
    # This is used to map json getters to ruby getters.
    # Returns a Hash of form { json_getter_name_string => ruby_object_getter_name_string }
    # If a replacement is specified, it is used as a Ruby getter. The original getter name is used as the key in the JSON file.
    # A nil value means that the ruby getter is the same as the json getter.
    # Nil values are used when json_getters are derived from json_getter_overrides (which lets you specify the marshalling behavior by
    # defining a single method instead of two.)
    def json_getter_overrides
      overrides = JSON_Graph_State.json_getter_overrides_by_class[self]  
      return overrides if overrides
      try_super = superclass && superclass.respond_to?(:json_getter_overrides) 
      return superclass.json_getter_overrides if try_super
      Hash.new
    end
    # Returns { json_getter => ruby_getter } for those json_getters that actually get used.
    # Nil values are replaced (value is same as key if there is a nil to specify no translation).
    # Takes into account ignored_getters and ignored_getters_by_class.
    # Does not take into account object specific filtering by getter_tests or getter_tests_by_class.
    def getter_mapping
      answer = Hash.new
      overrides = json_getter_overrides.clone
      unignored_json_getters.each{|json_getter| answer[json_getter] = overrides[json_getter] || json_getter}
      answer
    end
    # Returns ruby getters used on instances of classes that have this method.
    # Takes into account ignored_getters and ignored_getters_by_class.
    # Does not take into account object specific filtering by getter_tests or getter_tests_by_class.
    def ruby_getters; getter_mapping.values; end
    # These are the domain object getters that the class knows can be used during marshalling.
    # This does not include object specific filtering by getter_tests or getter_tests_by_class
    # The difference between this and json_getters is that this takes ignored_getters and ignored_getters_by_class into account
    def unignored_json_getters
      # puts "Calling unignored_json_getters"
      json_getters - ignored_json_getters
    end
     # This does not include object specific filtering by getter_tests or getter_tests_by_class
    def ignored_json_getters
      ret = JSON_Graph_State.ignored_getters + (JSON_Graph_State.ignored_getters_by_class[self] || [])
      # puts "Ignored getters for #{self} are #{ret}"
      ret
    end
  end
    
  
  # Use "include" to mix into any complex class whose instances are to be marshalled.
  # Then you must either:
  #   * Define a json_getter_overrides_by_class entry for the base (mixed-into) class, and ignore both json_getter_overrides and json_getters.
  #       PLEASE USE THIS METHOD unless you've got a darned good reason for another choice.
  #       This is now the preferred method, because it lets you specify the entire traversal sequence and marshalled form in one place.
  #       This also makes it easy to define different traversals for different purposes.
  #       The entry in json_getter_overrides_by_class should have a key of all object getters that are to be traversed. Use nil for the Hash's value of any getters that don't need translation.
  #   * Define on the class side json_getter_overrides, and ignore json_getters
  #        This is the best choice for specifying marshalling information in each domain class (instead of in a single json_getter_overrides_by_class Hash).
  #        Include all getters: use nil for the value of any getters that don't need translation
  #        If you do this in addition to specifying json_getter_overrides_by_class, then your version of json_getter_overrides will take precidence.
  #        That's because json_getter_overrides_by_class is referenced only by the default behavior of json_getter_overrides.
  #   * Define on the class side json_getters, and ignore (json_getter_overrides and json_getter_overrides_by_class)
  #        This is the best choice when you don't mind adding methods to the domain classes, and the getters don't neeed translation (object getters are the same as the json attribute names).
  #        Ignoring json_getter_overrides and json_getter_overrides_by_class results in an empty json_getter_overrides Hash.
  #   * Define on the class side BOTH:
  #        * json_getters (including all the getters, both the ones that need translation and the ones that don't need translation) 
  #        * json_getter_overrides (including only those getters that need translation)
  #        Define both you don't mind specifying marshalling behavior in two methods on domain object classes, and you've only got a few getters that need translation.
  #        When both methods are defined, json_getters will determine which getters are used, and json_getter_overrides will remap whichever getters you include there.
  # In all cases, further filtering is provided by:
  #   * ignored_getters
  #   * ignored_getters_by_class
  # Please note that:
  #   * The class side versions of json_getter_overrides and json_getters are now required, because they are needed for XML Schema generation.
  #   * Instance side versions of json_getter_overrides and json_getters are used during code marshalling.
  #   * Instance side versions can do instance specific stuff - but the schema will not know about it.
  #     * Instance side versions should always returna subset of the class side versions
  #     * Instance side versions can be thought of as use case specific marshalling requirements
  #   * The decision which technique to use can be applied at the class level (that's not reccomended, but you can do it). 
  #     For example, you can use json_getter_overrides_by_class for class Foo, use json_getter_overrides for class Bar, and use json_getters for class Baz.
  # Optionally you can:
  #   * Define instance side versions of json_getters and json_getter_overrides, to modify at the instance level what gets marshalled.  This will not be reflected in schemas.
  #   * Define Procs that make instance level decisions about which getters are marshalled:
  #     * getter_tests
  #     * getter_tests_by_class
  # How it works:
  #   * The mixin defines
  #     * a default class side json_getter_overrides which gets values from JSON_Graph_State.json_getter_overrides_by_class
  #     * a default class side json_getters, which returns the keys of the class side json_getter_overrides
  #     * default instance side json_getter_overrides and json_getters, which invoke the class side behaviors
  module Serializeable
    def self.included(base)
      base.extend Serializeable_Class_Side
    end
    # See comment in Serializeable_Class_Side
    def json_getters; self.class.json_getters; end
    # See comment in Serializeable_Class_Side
    def json_getter_overrides; self.class.json_getter_overrides; end
    # This should be overridden to provide a guaranteed unique id.
    # This version defaults to object_id, which works in most cases. A counter-example could be different objects
    # that represent the same row in a relational database (the json_id in that case should be composed from the table
    # name and the values of the primary keys).
    # See comments in JSON_Graph_State
    def json_id; object_id; end
    # Returns the receiver in the form of a Hash of data to be serialized. 
    # This strips out attributes that are not to be serialized, and it converts attribute names based on json_getter_overrides.
    # Note that the default is not not recurse.
    def to_marshalling_form(max_depth=1, current_depth=0, marshalling_options = {})
      allow_duplicates = marshalling_options[:allow_duplicates] || false
      stop_cycles = marshalling_options[:stop_cycles] || true
      # tag_is_class = marshalling_options[:tag_is_class] || false
      current_depth += 1
      raise "Called to_marshalling_form with arguments that do not allow for a single execution" if 0!=max_depth && current_depth > max_depth
      recurse = 0==max_depth || current_depth < max_depth
      marshalling_id = JSON_Graph_State.marshalling_id_for(self)
      class_name = self.class.name.to_java_pkg_path
  # puts ">>> #{id.inspect}"
      is_duplicate = false
      if JSON_Graph_State.seen[marshalling_id]
        if allow_duplicates
          is_duplicate = true
          # then what
        else
          # If we proceeded anyway when we can't encode a reference, there would be a danger of endless recursion
          raise "JSON marshalling has already seen an instance of #{class_name} with marshalling_id #{marshalling_id}, but references are disabled, and therefore cannot be encoded." unless JSON_Graph_State.references?
          return {JSON_Graph_State.classname_key => class_name,  JSON_Graph_State.ref_key => marshalling_id }
        end
      end
      JSON_Graph_State.seen[marshalling_id]=self
      hashed = {}
      hashed[JSON_Graph_State.classname_key] = class_name if JSON_Graph_State.classname?
      hashed[JSON_Graph_State.id_key] = marshalling_id if JSON_Graph_State.references?
      proceed = !(is_duplicate && stop_cycles)
      if proceed
        # puts "json_getter_overrides for #{self.class}:"; pp JSON_Graph_State.json_getter_overrides_by_class[self.class]
        # puts "\n#{self.class} json_getters: #{self.json_getters.sort}"
        json_getters.each do |json_getter| 
          next if JSON_Graph_State.ignore_getter?(json_getter, self, hashed)
          ruby_getter = json_getter_to_ruby_getter(json_getter)
          # puts "actual_getter is #{actual_getter.inspect}"
          # test to see if we should continue with this based on the getter tests
          value = send_chained(ruby_getter).json_output_clean
          next if value.nil? && JSON_Graph.omit_nil_attributes?
          if json_getter == :code
            puts "json_getter is :code and ruby_getter is #{ruby_getter}\n   #{json_getter_overrides}\n      value: #{value}\n\n"
          end
          # puts "value: #{value.inspect}\n\n"
          value = value.to_marshalling_form(max_depth, current_depth, marshalling_options) if recurse
          hashed[json_getter.to_s] = value
        end
      end
      hashed
    end
    def json_getter_to_ruby_getter(json_getter)
          jgo = json_getter_overrides[json_getter.to_s] || json_getter_overrides[json_getter] # why limit to strings??
          jgo || json_getter
    end
    def json_getter_to_ruby_setter(json_getter)
          ruby_getter = json_getter_to_ruby_getter(json_getter)
          "#{ruby_getter}="
    end
    def to_json(*args); to_marshalling_form.to_json(*args); end  # note that default of to_marshalling_form is to not recurse
    # Recursively fill the receiver with data from data_hash
    def json_populate(data_hash, state)
      data_hash.each {|json_getter, raw_value|
        next if JSON_Graph_State.ignore_getter?(json_getter, self, data_hash)
        cooked_value = raw_value.to_json_objs(state)
        next unless nil!=cooked_value
        ruby_setter = json_getter_to_ruby_setter(json_getter)
        send(ruby_setter, cooked_value)
        }
    end
  end
  
=begin
  # HISTORICAL - NO LONGER USED
  # Need to use "extend" to mix into any complex class that is to be instanted during unmarshalling.
  module Instantiable
    # Class side behavior to create a new instace.
    # This DOES NOT WORK because it presumes that objects are defined (and registered) before they are referenced.
    # That's true, so far as the order in which hashes are encountered (because of how to_json is coded)... but
    # the JSON parser builds objects from the bottom up (so that json_create can be passed a hash with data in it).
    # Alternatives:
    #   * Modify JSON to create an empty container first (registration/dereferencing would occur here), populate data after "innards" have been parsed.
    #     Difficulty: you still need to process the innards to figure out which class to instantiate
    #   * Use two passes. First pass creates Hashes.  Second pass converts the Hashes to instances.
    def self.json_create(hash)
      cls_name = hash.delete(JSON_Graph_State.classname_key)  # this served it's purpose by identifying the class this message was sent to. The no-longer-necessary key is not automatically removed
      ref = hash.delete(JSON_Graph_State.ref_key)
      inst = JSON_Graph_State.seen[ref]
      return inst if inst
      raise "Failed to find an instance of #{cls_name} referenced by #{ref.inspect}" if ref
      # Need to create a new instance
      id = hash.delete(JSON_Graph_State.id_key)
      raise "Encountered an instance of #{cls_name} without an id: unable to register it, so it can't be referenced later." unless id
      inst = self.new
      hash.each {|getter, value|
        setter = "#{getter}="
        inst.send(setter, value) if value
        }
      JSON_Graph_State.seen[id]=inst
      inst
    end
  end
=end

end

class Object
  def to_json_objs(state=JSON_Graph_State.instance); self; end
end

class Array
  def to_json_objs(state=JSON_Graph_State.instance)
    # Arrays cannot be referenced from another part of the file: there is no way to specify an id for them.
    collect {|element| element.to_json_objs(state) }
  end
end

# This (and methods like it) are defined elsewhere. Need to consolodate them. And add a method that throws an error if the constant isn't a Class.
# Raises a NameError if the spcified constant cannot be found
def qualified_const_get(path)
  steps = path.to_s.split('::')
  # It's counter intuitive, but for many purposes Object is the top level namespace (I'd think Kernel would be better)
  steps.inject(Object) { |ns,name| 
    begin
      ns.const_get(name)
    rescue NameError => e
      # maybe the last argument is a method...
      method, *args = name.split(/[\(\)\s+,]/)
      ns.respond_to?(method) ? (ns.send method, args) : (raise e)
    end
  }
end


class Hash
  # A misnomer - recursively converts a Hash version of an object into a Ruby version of the object.
  def to_json_objs(state=JSON_Graph_State.instance)
      seen = state.seen
      ref = JSON_Graph_State.references? ? delete(JSON_Graph_State.ref_key) : nil
      inst = ref ? seen[ref] : nil
      return inst if inst
      cls_name = delete(JSON_Graph_State.classname_key).to_ruby_pkg_path
      raise "Invalid file: failed to find an instance of #{cls_name.inspect} referenced by #{ref.inspect}" if ref
      # Need to create a new instance
      marshalling_id = JSON_Graph_State.references? ? delete(JSON_Graph_State.id_key) : nil
      unless cls_name
        # We have a Hash. Hashes will usually not have ids. Accomodate the possibility anyway.
        seen[marshalling_id]=self if marshalling_id
        # Remove unwanted keys. This:
        #    JSON_Graph_State.ignored_getters.each {|ignored| delete(ignored)}
        # is equivalent to the following elsewhere:
        #    next if JSON_Graph_State.ignore_getter?(getter, self)
        # because we do not have a class name. There are only global ignores
        JSON_Graph_State.ignored_getters.each {|ignored| delete(ignored)}
        # Convert the contents of the hash
        keys.each {|key| self[key]= self[key].to_json_objs(state) }
        return self
      end
      raise "Invalid file: encountered an instance of #{cls_name} without an id: unable to register it, so it can't be referenced later." if JSON_Graph_State.references? && !marshalling_id
      # We need to instantiate a class. Create it and register it before converting contents of receiver (so that references can be found)
      inst = qualified_const_get(cls_name).create_empty_instance  # Should verify this constant is a class... see comment on qualified_const_get
      seen[marshalling_id]=inst if marshalling_id
      # Set data in the instance
      inst.json_populate(self, state)
      inst
  end
end

class Class
  alias create_empty_instance allocate
end