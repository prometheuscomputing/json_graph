require 'sequel_specific_associations/additional_extensions'


# See instructions in JSON_Graph::Serializeable
class Sequel::Model
  include JSON_Graph::Serializeable
  def json_id; "#{self.class.name}_#{id}"; end
  def self.create_empty_instance; create; end
end
