require 'json_graph/sequel_concise_marshalling'


# This adds a default class-level json_getters, which may be satisfactory under some circumstances.
# Art prefers to specify the entire json/ruby mapping in json_getter_overrides_by_class, and use default
# implementations of json_getters and json_getter_overrides
class Sequel::Model
  def self.json_getters
    ret = all_property_names(:content) - JSON_Graph_State.excludes_for_class(self)
    # puts "JSON getters for #{self} are #{ret}"
    # ret
  end
  def json_getters; self.class.json_getters; end
end
