require 'date'
require 'time'

class DateTime
  # NOTE: to_s isn't accurate enough since it doesn't include fractions of a second.
  #       This causes a serialized/deserialized Time to be different from the original
  #       %9N adds the fractional part of the second in nanoseconds to the string
  def to_s_with_nsec; strftime("%Y-%m-%d %H:%M:%S:%9N %z"); end
  # NOTE: The below format should use "%9N" instead of "%N", but strptime won't accept the format.
  #       The specific (and misleading) error is: ArgumentError: invalid date
  #       Since strptime defaults to using %9N when %N is specified, this isn't currently a problem.
  def self.from_s_with_nsec(str);
    begin
      DateTime.strptime(str, "%Y-%m-%d %H:%M:%S:%N %z")
    rescue ArgumentError # Attempt fallback to generic DateTime parse
      DateTime.parse(str)
    end
  end
end

class Time
  # NOTE: to_s isn't accurate enough since it doesn't include fractions of a second.
  #       This causes a serialized/deserialized Time to be different from the original
  #       %9N adds the fractional part of the second in nanoseconds to the string
  def to_s_with_nsec; strftime("%Y-%m-%d %H:%M:%S:%9N %z"); end
  # NOTE: The below format should use "%9N" instead of "%N", but strptime won't accept the format.
  #       The specific error is: ArgumentError: invalid strptime format
  #       Since strptime defaults to using %9N when %N is specified, this isn't currently a problem.
  def self.from_s_with_nsec(str)
    begin
      Time.strptime(str, "%Y-%m-%d %H:%M:%S:%N %z")
    rescue ArgumentError # Attempt fallback to generic Time parse
      Time.parse(str)
    end
  end
end

class Date
  # Convenience method for consistency with Time and DateTime
  def self.from_s(str); Date.parse(str); end
end