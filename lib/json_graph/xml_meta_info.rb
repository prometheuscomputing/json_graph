require 'date'

# See also the instantiation of Transformer instances in pippin/roog.rb
RUBY_SCHEMA_TYPE_MAP = {
  Symbol => ["xsd", "string"],
  String => ["xsd", "string"],
  TrueClass => ["xsd", "boolean"],   # Sequel (and therefore SSA) uses TrueClass to specify all Booleans.
  FalseClass => ["xsd", "boolean"],
  Fixnum => ["xsd", "integer"],      # The Ruby language does not specify the max size of a Fixnum (it depends on the hardware)
  Bignum => ["xsd", "integer"],
  Integer => ["xsd", "integer"],     # Integer is abstract, but it's sensible for SSA to specify it as a return type, so it needs to be in the list
  Time => ["xsd", "time"],
  DateTime => ["xsd", "dateTime"],
  Float => "xsd:double"              # Ruby floats are double precision
}

if Object.const_defined?('Gui_Builder_Profile::RichText')
  RUBY_SCHEMA_TYPE_MAP[Gui_Builder_Profile::RichText] = ["xsd", "string"]
end



class NilClass
  # I'm not certain this is the right default.
  def multiplicity; Range.new(0, 1); end
  def to_classifier; nil; end
end

class Symbol
  def multiplicity
    # We lack information about the lower bound
    case self
      when :one_to_one; Range.new(0, 1)
      when :many_to_one; Range.new(0, 1)
      when :one_to_many; Range.new(0, Float::INFINITY)
      when :many_to_many; Range.new(0, Float::INFINITY)
      else raise "Unexpected multiplicity: #{self.inspect}"
    end
  end
end



class Module
  def to_classifier; self; end
  # Returns nil if not found
  def containing_path_or_nil; name.containing_path; end
  # Raises error if not found
  def containing_path
    answer = name.containing_path
    raise "Failed to find containing path for #{self.inspect}" unless answer
    answer
  end
  def includes?(other_module)
    included_modules.member?(other_module)
  end
  def directly_contained_modules
    const_values = constants.collect {|name| const_get(name) }
    mods = const_values.select {|value| value.kind_of?(Module)}
    # Our infrastructure uses aliases, so that (for example), 
    # Gui_Builder_Profile::RegularExpressionCondition has an Onc::RegularExpressionCondition alias.
    # We want to filter those out: they will be defined in the package they really belong to.
    mods.select {|mod| mod.containing_path == self.name }
  end
  # True if it contains other Modules (including classes).  If false, we consider it an interface.
  def represents_package?
    ! directly_contained_modules.empty?
  end
end

class String
  def to_classifier
    Kernel.const_get(self)  # This requires a recent version of Ruby that can handle '::' delimited paths
  end
  # The receiver is expected to be a '::' delimited package path.
  # The result is nil if the receiver does not contain a '::' separator.
  def containing_path
    path_and_name.first
  end
  # The receiver is expected to be a '::' delimited package path.
  # Returns [container_path, name]
  # The path:
  #  * is nil if the receiver name does not contain a '::' separator  (for example, Integer)
  #  * the '::' delimited path of the package that holds name
  # This does not consider the validity of the path (which could be entirely bogus).
  # "Frabjous::day::Callo::Callay".path_and_name  # Returns ["Frabjous::day::Callo", "Callay"]
  # "Frabjous".path_and_name  # Returns [ nil, "Frabjous"]
  def path_and_name
    steps = split('::')
    name = steps.pop
    path = steps.empty? ? nil : steps.join('::')
    [path, name]
  end
    
end

class Object
  def enumeration?; false; end
end

# Mix into the class side (by extension) of enumerations.
module Enumeration
  # This creates a String subclass that implements #enumeration? and #enumeration_values.
  # The created String subclasses are added to Enumeration.implementors.
  # Examples:
  #    module Piggy; end
  #    Enumeration.create :'Piggy::HousingMaterial', 'Straw', 'Sticks', 'Bricks', 'Titanium'
  #    Enumeration.create :Marsupials, 'Kangaroo', 'Koala', 'Duck-billed platypus'
  #    Piggy::HousingMaterial.enumeration_values
  #    Marsupials.enumeration_values
  def self.create(enum_name, *enum_values)
    code = 
    "class #{enum_name} < String
       extend Enumeration
     end
     #{enum_name}.enumeration_values= #{enum_values.inspect}
     Enumeration.implementors << #{enum_name}"
     Object.module_eval code
  end
  def self.implementors; @implementors||=Array.new; end
  def enumeration?; true; end
  def enumeration_values; @enumeration_values||=Array.new; end
  def enumeration_values=(array); @enumeration_values=array; end
  alias_method :literal_values, :enumeration_values
end

# Mix into the class side (by extension) of modules that represent interfaces.
# Then include the interface in any class that implments the interface.
# Examples:
#   module Marsupial; extend Interface; end
#   class Wombat; include Marsupial; end
#   Marsupial.implementors # returns an Array containing class Wombat
module Interface
   def implementors; @implementors||=Array.new; end
   def included(base); implementors << base; end
end



# Each instance contains information about an attribute or association. 
# Instances are built (and used) during XML Schema generation. 
# Instances are cached in a registry when they created, used again later during XML instance parsing.
# This information could be extracted from an XML Schema. Instead it's from SSA metamodel hashes. 
# I'd like to see this as part of SSA metamodel.
class Reference_Info
  # Containing class
  attr_accessor :holder
  # From getter_mapping
  attr_accessor :json_getter, :ruby_getter
  # From SSA :class
  attr_accessor :classifier
  # Fram SSA :type
  attr_accessor :multiplicity
  # From SSA
  attr_accessor :metainfo
  # Either :association, :attribute, or :root
  attr_accessor :type
  # ========================
  # The following are used by XML instance parsing, but not schema generation
  # ========================
  # True if the association or attribute is inherited
  attr_accessor :inherited
  # Strings
  attr_accessor :namespace_uri, :namespace_prefix
  
  def self.clear
    # puts "CLEARING REGISTRY"
    registry.clear
  end
  def self.registry; @registry||=Hash.new; end
  def registry; self.class.registry; end
  
  def self.show_registry
    puts "\nRegistry:"
    registry.keys.each {|key| puts "\t#{key.inspect}"}
    puts
  end
 
  # Returns all the instances needed to describe a given klass in a schema.
  # Returns an array of arrays:
  #   0. Array of instances that contain information about associations
  #   1. Array of instances that contain information about attributes
  # The above arrays do not contain instances that have inherited=true.
  # Even though instances with inherited=true are not returned, inherited instances are created if they don't exist (because they are needed during XML parsing)
  def self.instances(klass, namespace_prefix, namespace_uri)
    association_metainfos = klass.associations
    attribute_metainfos = klass.attributes
    parent_association_metainfos = Hash.new
    parent_attribute_metainfos = Hash.new
    klass.parents.each {|parent|
      parent_association_metainfos.merge! parent.associations
      parent_attribute_metainfos.merge! parent.attributes
      }
    associations = Array.new
    attributes = Array.new
    klass.getter_mapping.each {|json_getter, ruby_getter|
      assoc_info = association_metainfos[ruby_getter.to_sym]
      if assoc_info
        # ignore inherited associations
        inherited = nil!=parent_association_metainfos[ruby_getter.to_sym]
        type = assoc_info[:class]
        raise "Class #{klass.name} association info for getter #{ruby_getter} did not contain :class" unless type
# puts "Class #{klass.name} json_getter #{json_getter} ruby_getter #{ruby_getter} is an association that returns type #{type.inspect}. Inherited = #{inherited.inspect}"
        classifier = type.to_classifier
        multiplicity = assoc_info[:type].multiplicity
        inst = find_or_create(klass, json_getter, ruby_getter, classifier, multiplicity, assoc_info, :association, inherited, namespace_prefix, namespace_uri)
        associations << inst unless inherited
      else
        # ignore inherited attributes
        inherited = nil!=parent_attribute_metainfos[ruby_getter.to_sym]
        attrib_info = attribute_metainfos[ruby_getter.to_sym]
        raise "Class #{klass.name} failed to have either association or attribute info for getter #{ruby_getter}" unless attrib_info
        type = attrib_info[:class]
        raise "Class #{klass.name} association info for getter #{ruby_getter} did not contain :class" unless type
# puts "Class #{klass.name} json_getter #{json_getter} ruby_getter #{ruby_getter} is an attribute that returns type #{type.inspect}. Inherited = #{inherited.inspect}"
        classifier = type.to_classifier
        # When classifier == Array, it's a fake, added to the marshalling specification to represent an Array of Strings.
        # We will treat this as an assocciation to String
        if Array==classifier
          multiplicity = Range.new(0, Float::INFINITY)
          inst = find_or_create(klass, json_getter, ruby_getter, String, multiplicity, attrib_info, :attribute, inherited, namespace_prefix, namespace_uri)
          associations << inst unless inherited
        else
          multiplicity = Range.new(0, 1)
          inst = find_or_create(klass, json_getter, ruby_getter, classifier, multiplicity, attrib_info, :attribute, inherited, namespace_prefix, namespace_uri)
          attributes << inst unless inherited
        end
      end
      }
    [associations, attributes]
  end

  # Registers new instances. Raises exception if instance already exists.
  def initialize(holder, json_getter, ruby_getter, classifier, multiplicity, metainfo, type, inherited, namespace_prefix, namespace_uri)
    json_getter = json_getter.to_sym
    ruby_getter = ruby_getter.to_sym if ruby_getter
    self.holder=holder
    self.json_getter=json_getter
    self.ruby_getter=ruby_getter
    self.classifier=classifier
    self.multiplicity=multiplicity
    self.metainfo=metainfo
    self.type=type
    self.inherited=inherited
    self.namespace_prefix=namespace_prefix
    self.namespace_uri=namespace_uri
    key = [holder, namespace_uri, json_getter]
    raise "Unexpectedly tried to create duplicate Reference_Info: #{self.inspect}" if registry[key]
    registry[key]=self
    # puts "ADDED TO REGISTRY: #{key.inspect}"
  end
  
  # Attempts to find an instance.  Raises exception if instance can't be found.
  def self.lookup(holder, namespace_uri, json_getter)
    json_getter = json_getter.to_sym
    key = [holder, namespace_uri, json_getter]
    inst = find(*key)
    unless inst
      keys = registry.keys.collect {|key| key.inspect}
      keys_str = keys.join("\n\t")
      raise "Expected (but failed) to find a Reference_Info instance for key #{key.inspect}\nValid keys are:\n\t#{keys_str}"
    end
    inst
  end
  
  # Attempts to find an instance.  Returns nil if an existing instance can't be found.
  def self.find(holder, namespace_uri, json_getter)
    json_getter = json_getter.to_sym
    key = [holder.to_classifier, namespace_uri, json_getter]
    registry[key]
  end
  
  # Attempts to find an instance.  If it can't find one, creates a new instance and Registers it. Does not return nil.
  def self.find_or_create(holder, json_getter, ruby_getter, classifier, multiplicity, metainfo, type, inherited, namespace_prefix, namespace_uri)
    json_getter = json_getter.to_sym
    holder = holder.to_classifier
    find(holder, namespace_uri, json_getter) || new(holder, json_getter, ruby_getter.to_sym, classifier, multiplicity, metainfo, type, inherited, namespace_prefix, namespace_uri )
  end
  
  def self.add_root(json_getter, classifier, namespace_prefix, namespace_uri)
    # puts "Adding root #{json_getter} in namespace_uri #{namespace_uri}"
    new(nil, json_getter, nil, classifier, Range.new(1, 1), nil, :root, false, namespace_prefix, namespace_uri)
  end
  
  def fixed_classifier
    Schema_Builder.enum_fix(holder, ruby_getter, classifier)
  end

end
