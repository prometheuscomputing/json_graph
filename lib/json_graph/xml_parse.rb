require 'oga'
require 'json_graph/xml_meta_info'


class Stack_Entry

  # A Hash
  attr_accessor :attributes
  # A Hash of form { prefix => namespace }.  Key and value are both strings
  # This needs to be maintained on the stack because every new element can have namespace declarations.
  attr_accessor :namespaces
  # A string
  attr_accessor :default_namespace
  # A Reference_Info
  attr_accessor :ref_info
  
  def initialize(default_namespace, namespaces, attributes, ref_info)
    self.default_namespace=default_namespace
    self.namespaces=namespaces
    self.attributes=attributes
    self.ref_info=ref_info
  end
  
  def classifier; ref_info.classifier; end
  def json_getter; ref_info.json_getter; end
  def collection?; ref_info.multiplicity.max > 1; end
  
end


class XML_Graph
  
  attr_accessor :parser, :stack, :root_obj
  
  
  
  def self.path2obj(path)
    new.path2obj(path)
  end
  
  def self.string2obj(string)
    new.string2obj(string)
  end
  
  def self.path2hash(path)
    new.path2hash(path)
  end
  
  def self.string2hash(string)
    new.string2hash(string)
  end
  
  def clear_parser
    self.root_obj = nil
    self.stack = Array.new
  end
  
  # Given a path to an XML file that describes an object graph, returns the object corresponding to the root.
  def path2obj(path)
    hash = path2hash(path)
    # pp hash
    hash.to_json_objs
  end
    
  # Given an XML string that describes an object graph, return the root of the graph.
  def string2obj(string)
    string2hash(string).to_json_objs
  end
  
  # Given a path to an XML file that describes an object graph, returns the Hash corresponding to the root object.
  def path2hash(path)
    string = File.read(path)
    string2hash(string)
  end
    
  # Given an XML string that describes an object graph, returns the Hash corresponding to the root object of the graph.
  def string2hash(string)
    clear_parser
    # XML_Graph is used as the handler
    self.parser = Oga::XML::SaxParser.new(self, string)
    parser.parse
    root_obj
  end
   
  def tos; stack.last; end

  
  # ====================================================
  # SAX handler callback methods
  
  def on_element(namespace_prefix, name, attrs = {})
    namespaces, raw_attributes = attrs.partition_namespaces
    default_namespace = raw_attributes.delete('xmlns')
    # puts "Start Element  namespace_prefix: #{namespace_prefix.inspect}  name: #{name.inspect}   raw_attributes: #{raw_attributes.inspect}  namespaces: #{namespaces.inspect}  default_namespace = #{default_namespace}"
    holder = nil
    t = tos
    if t
      default_namespace = default_namespace || t.default_namespace
      namespaces = t.namespaces.merge(namespaces)
      holder = t.ref_info.classifier
    end
    namespace_uri = namespaces[namespace_prefix]
    ref_info = Reference_Info.lookup(holder, namespace_uri, name)
    attributes = {JSON_Graph_State.classname_key => ref_info.classifier.name}
    entry = Stack_Entry.new(default_namespace, namespaces, attributes, ref_info)
    stack.push(entry)
    raw_attributes.each {|attr_name, raw_value|
      # Convert attribute value strings into integers, floats, booleans, etc.
      r = Reference_Info.lookup(ref_info.classifier, namespace_uri, attr_name)
      c = r.classifier
      # puts ">>>>>> name: #{attr_name}   value: #{raw_value.inspect}    classifier: #{c.inspect}"
      # puts "       Reference_Info: #{r.inspect}"
      # Reference_Info.show_registry
      attributes[attr_name]=c._from_xml(raw_value)
    }
  end
  
  def after_element(namespace_prefix, name)
    name = name.to_sym
    entry = stack.pop
    raise "Unbalanced stack: entry was #{entry.json_getter.inspect}, but after_element name was #{name.inspect}" unless entry.json_getter==name
    attributes = entry.attributes
    if tos
      if entry.collection?
        # If this is a collection, we need to make certain the value is an Array.
        # We don't know if this is the first occurence of this element, so we need to check
        collection = tos.attributes[entry.json_getter]||=Array.new
        collection << attributes
      else
        tos.attributes[entry.json_getter]=attributes
      end
    else
      self.root_obj = attributes
    end
    # puts "End Element  namespace_prefix: #{namespace_prefix.inspect}  name: #{name.inspect}   entry: #{entry.inspect}"
  end

end

class Hash
  
  NAMESPACE_REGEX = /^xmlns:(\S+)/
  
  # Returns two sub-hashes.  The first contains namespaces.  The second contains non-namespaces.
  def partition_namespaces
    namespaces = Hash.new
    attributes = Hash.new
    each {|key, value|
      match_data = NAMESPACE_REGEX.match(key)
      match_data ? namespaces[match_data[1]]=value : attributes[key]=value
      }
    return namespaces, attributes
  end
end
