require 'builder'
require 'json_graph/xml_meta_info'

=begin

Interfaces are problematic for object-XML mapping.
There are several alternatives, none entirely satisfactory.
The main problem is that they work at the element level: 
they let you subsitute one element for another (instead of one
type for another).  Substituting elements (instead of types)
amounts to substituting roles (instead of interface implmenting classes).

Substitution groups:
* Lets a class in package X implement an Interface defined in package Y.
  This behavior is expected of UML interfaces.
* A class cannot implement two different interfaces (unless one inherits from the other).
  This can be worked around by creating additional interfaces (which are not meaningful in the problem domain).
* You need to define a subsitution group "head": <xsd:element name = "InterfaceName" abstract = "true"/>
* Then for every element you can substitute: <xsd:element name = "ImplName" type = "ImplClassName" substitutionGroup = "InterfaceName"/>
* You then use the head in the schema, and can use any substitute in your instance
* The head can itself be a member of some larger subtitution group
* The elements that subsitute for the head are (unfortunately) global.
  They can be (for example) used as a root element.

Reuseable choice groups:
* All the choices need to be available in the schema that defines the group.
  To add another element (in a different schema), you need to redefine the group (which is ugly).
* A class can implement multiple interfaces
* You define a group <xsd:group name="InterfaceName"> that contains <xsd:choice>
* Then for every element you can substitute, add an element to the choice: <xsd:element name = "ImplName" type = "ImplClassName"/>
* Classes that associate to the interface are represented by complex types
  that have a reference to the group (instead of an element): <xsd:group ref="InterfaceName">

Non-reusable choice groups:
* Adding new choices in a different schema is even harder than reuseable choice groups,
  because you need to redefine all the types that are associated to the interface.
* A class can implement multiple interfaces
* The choices are individually coded into each complexType that uses them
* The role is represented by an <xsd:choice> element in the associated complexType
* The <xsd:choice> then contains an element for every implmenter: <xsd:element name = "ImplName" type = "ImplClassName"/>
* The <xsd:choice> needs to be redefined for every association to the interface.

This implmentation regards reusable choice groups as the lesser evil.

=end

# Specifies information about one XML Schema. Also builds the schema.
# Instances are tracked, and can be looked up by package path.
# There are several ways to create instances, depending on if you want to assign namespaces to the corresponding schemas.
# * Use Schema_Builder.root_classes = Array_of_classes #  Automatically create instances, using default namespaces
#   Optionally Schema_Builder.find(pkg_path) or Schema_Builder.instance(pkg_path) to get instances created automatically, and then modify namespaces, options, etc.
#   You should not modify the root_class if the schema represents a component used by other schemas (specify the marshalling roots only for schemas that represent messages, not components).
# * Use Schema_Builder.instance(package_path, root_classes=[], namespace_prefix=nil, namespace_uri=nil, options={})
#   If the instance has already been created as a side effect of some other instance creation, "instance" will updated it.
# * You should NOT directly send 'new' to the class. You will get an exception if the instance for the path already exists.
#   That's because instances are tracked and duplicates are not permitted. Schema_Builder.instance does not have this problem, because it can return an existing instance.
class Schema_Builder
  
  # Problems with multiple instances of Builder::XmlMarkup
  # This originally had an individual Builder::XmlMarkup (and individual content).
  # That lets you set options differently on each Builder::XmlMarkup.
  # Unfortunately, only the first Builder::XmlMarkup was actually writing XML.
  # A single Builder::XmlMarkup and a single content (target) are now being used.
  # The content is cleared at the beginning of schema generation.
  # The at the end of generation, the content is cloned and the clone is stored in schema.
  
  # === Configuration =====
  # This really ought to be specified in advance. You can get by with just the package_path though.
  
  # The :: delimited path of ruby modules required to reach the module in question.
  attr_accessor :package_path
  # A Symbol and String that define the targetNamespace
  attr_accessor :namespace_prefix, :namespace_uri
  # An array of Classes that may be represented by root elements.
  attr_accessor :root_classes
  # A Booleaon. Defaults to class side default_include_object_id
  attr_accessor :include_object_id
  # May include a copyright. Defaults to class side default_top_comment. If nil, there is no comment.
  attr_accessor :top_comment
  # Used in computing any namespace urls that are not explicitly provided
  attr_accessor :organization
  
  # ====== Policy/configuration attributes that can be passed in as options, with defaults defined by constants =========
  #        These could be specified differently for each schema, but that wouldn't be very consistent
  
  # A Hash, default value is DEFAULT_SCHEMA_ATTRIBUTES
  attr_accessor :schema_attributes
  # These are Booleans that default to DEFAULT_USE_COLLECTION_CONTAINERS and DEFAULT_ALWAYS_KEEP_COMPOSITIONS.
  attr_accessor :use_collection_containers, :always_keep_compositions
  
  # === State =====
  
  # Module that corresponds to :package_path
  attr_accessor :mod
  # A Hash. This is passed on to any other instances that this instance needs to create.
  attr_accessor :builder_options
  # An array of classes directly contained in this package
  attr_accessor :directly_contained_classes
  # An array of enumeration classes directly contained in this package
  attr_accessor :directly_contained_enumerations
  # An Array of non-class, non-interface modules directly contained in this package
  attr_accessor :directly_contained_packages
  # An Array of non-class, non-package modules directly contained in this package
  attr_accessor :directly_contained_interfaces
  # These are paths to packages that contain classifiers associated with directly_contained_classes.
  attr_accessor :referenced_package_paths
  # True if instances are available for all directly_contained_packages and all referenced_package_paths
  attr_accessor :resolved
  # An Array of classifiers. Meaningful only for instances that have root_classes.
  # The reason is that the packages/schemas we reference may also be used by some
  # other schema, and we therefore must presume that everything in it that's public could be used.
  # We currently don't pay enough attention to what's public and what isn't. That
  # information should be exposed by the metainfo API.
  attr_accessor :root_reachable
  
  # See comment that begins with "Problems with multiple instances of Builder::XmlMarkup"
  # String containing schema being built.  Also called target.
  # attr_accessor :content
  # A Builder::XmlMarkup
  # attr_accessor :builder
  
  OPTION_KEYS = [:schema_attributes, :use_collection_containers, :always_keep_compositions]    
  DEFAULT_SCHEMA_ATTRIBUTES = { 'xmlns:xsd' => "http://www.w3.org/2001/XMLSchema", "elementFormDefault" => "qualified" }
  DEFAULT_USE_COLLECTION_CONTAINERS = false
  DEFAULT_ALWAYS_KEEP_COMPOSITIONS = false
  DEFAULT_ORGANIZATION = 'prometheuscomputing.com/'
  IGNORED_SUPERCLASSES = ['BasicObject', 'Object', 'Sequel::Model']
  PROMETHEUS_COPYRIGHT = "
Copyright (c) 2015 Prometheus Computing, LLC
Permission is hereby granted, free of charge, to any person obtaining a copy
of this schema and associated documentation files (the 'Schema'), to deal
in the Schema without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Schema, and to permit persons to whom the Schema is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Schema.
THE SCHEMA IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SCHEMA OR THE USE OR OTHER DEALINGS IN
THE SCHEMA.
"
  
  
  # =======================================================
  #  Class side behaviors
  # =======================================================
  
  def self.default_include_object_id; @default_include_object_id; end
  def self.default_include_object_id=(boolean); @default_include_object_id=boolean; end
  
  def self.default_top_comment; @default_top_comment; end
  def self.default_top_comment=(str); @default_top_comment=str; end
  
  def self.instance_hash; @instance_hash||=Hash.new; end
  
  def self.all_instances; instance_hash.values; end
  
  def self.resolved_classifiers; @resolved_classifiers||=Array.new; end
  
  def self.clear
    instance_hash.clear
    resolved_classifiers.clear
  end
  
  def self.resolved?(classifier)
    resolved_classifiers.member?(classifier)
  end
  
  def self.resolve
    all_instances.each {|inst| inst.resolve }
    all_instances.each {|inst| inst.find_root_reachable }
  end
  
  # Raises an exeption if an instance cannot be found for package_path.
  # For non-root schemas, use only after resolution (when all necessary instances should be present).
  # See also self.instance (which will create an instance if it does not already exist)
  def self.find(package_path)
    inst = instance_hash[package_path]
    raise "Failed to find Schema_Builder instance for package_path #{package_path.inspect}" unless inst
    inst
  end
    
  def self.prefix(package_path)
    find(package_path).namespace_prefix
  end
  
  # Used to identify enumerations.  A Hash of the form { [container_class, attribute_name] => replacement_type }
  def self.type_override; @type_override ||= Hash.new; end
  def self.type_override=(hash); @type_override=hash; end
  
  def self.enum_fix(container_class, attribute_name, original_type) 
    type_override[ [container_class, attribute_name.to_sym] ] || original_type
  end
  
  def self.root_classes=(klasses)
    klasses.each do |klass|
      path, name = klass.name.path_and_name
      raise "Class #{klass.name} is not defined in a module. Root classes need to be defined in some module (UML package) that corresponds to the XML Schema." unless path
      inst = instance(path)
      inst.root_classes << klass
    end
  end
  
  def self.make_schemas(dir=nil)
    resolve
    all_instances.each {|inst| inst.make_schema(dir) }
  end
  
  # =======================================================
  #  Instance creation
  # =======================================================
  
  # Returns an existing instance, if one exists. Otherwise creates a new instance.
  # See also self.find (for when you are certain the instance should already exist)
  def self.instance(package_path, root_classes=nil, namespace_prefix=nil, namespace_uri=nil, options={})
    inst = instance_hash[package_path]
    return new(package_path, root_classes, namespace_prefix, namespace_uri, options) unless inst
    inst.root_classes = root_classes if root_classes
    inst.namespace_prefix= namespace_prefix if namespace_prefix
    inst.namespace_uri= namespace_uri if namespace_uri
    inst.set_options(options, true)
    inst
  end
  
  # This creates an instance that knows directly contained stuff.
  # If root_classes are not specified, there will be no top level elements.
  # The root_classes are actual classes (not just names).
  # Options are discussed in #set_options
  def initialize(package_path, root_classes=nil, namespace_prefix=nil, namespace_uri=nil, options={})
    inst = self.class.instance_hash[package_path]
    raise "There is already a Schema_Builder for package_path #{package_path}. Use Schema_Builder.instance instead of Schema_Builder.new" if inst
    self.resolved = false
    self.include_object_id = self.class.default_include_object_id
    self.top_comment = self.class.default_top_comment
    self.root_reachable = Array.new
    self.root_classes = root_classes || Array.new
    self.class.instance_hash[package_path]=self
    self.referenced_package_paths = Array.new
    # See comment that begins with "Problems with multiple instances of Builder::XmlMarkup"
    # self.content = String.new
    set_options(options)
    self.package_path = package_path
    self.namespace_prefix = namespace_prefix || default_prefix
    self.namespace_uri = namespace_uri || default_uri
    self.mod = package_path.to_classifier
    const_values = mod.directly_contained_modules
    directly_contained_classifiers, self.directly_contained_packages = const_values.partition {|value| value.kind_of?(Class)}
    self.directly_contained_enumerations, classes = directly_contained_classifiers.partition {|value| value.enumeration? }
    self.directly_contained_interfaces, self.directly_contained_classes = classes.partition {|value| value.interface? }
  end
  
  # Returns a new option hash, ensures that a few required options are set.
  def normalize_options(options)
    normalized = options.clone
    normalized[:max_nesting] ||= 1000
    normalized[:indent] ||= 2
    # See comment that begins with "Problems with multiple instances of Builder::XmlMarkup"
    # normalized[:target] ||= content
    normalized
  end
  
  # Configure the receiver (and it's components)
  # Options are passed on to any other instance this instance creates.
  # Options can contain
  # * options for Builder::XmlMarkup.new
  # * options for this Schema_Builder, with keys specified in OPTION_KEYS
  #     If override is false, existing values are not changed.
  #     If override is true, and a value is provided, existing values are changed.
  #     If no value is present, and the options hash does not have a value, a default value is used.
  def set_options(options, override=false)
    options = normalize_options(options)
    self.builder_options = options
    OPTION_KEYS.each do |getter|
      setter = "#{getter}="
      value = options[getter]
      case
        when value && !override
          # do nothing
        when value && override
          send(setter, value)
        when !value
          value = self.class.const_get("DEFAULT_#{getter.upcase}")
          send(setter, value)
      end
    end
  end
  
  # =======================================================
  #  Derived accessors
  # =======================================================  
  
  def namespace=(hash)
    raise "Namespace hash must have a single prefic => uri entry. Keys are: #{hash.keys.inspect}" unless 1==hash.keys.size
    self.namespace_prefix = hash.keys.first
    self.namespace_uri = hash[namespace_prefix]
  end
  
  # See comment that begins with "Problems with multiple instances of Builder::XmlMarkup"
  def content
    @@content||=String.new
  end
  # See comment that begins with "Problems with multiple instances of Builder::XmlMarkup"
  def builder
    builder_options[:target]=content
    @@builder||=Builder::XmlMarkup.new(builder_options)
  end
  
  alias b builder # This makes code less readable. Usage of b replace with builder.
  alias target content
  
  def root_schema?; !root_classes.empty?; end
 
  def default_prefix
    ret = package_path.dup
    # without_vowels = ret.gsub!(/[aeiouAEIOU_ ]/, '')
    without_pkg_sep = ret.gsub!('::', '_')
    without_punctuation = ret.gsub!(/[!'":;<>\.,]/, '')
    ret
  end
  
  def default_uri
    "urn:#{organization || DEFAULT_ORGANIZATION}#{namespace_prefix}"
  end
  
  def filepath(parent_directory=nil)
    local = package_path.gsub('::', '/')
    local = "#{local}.xsd"
    return local unless parent_directory
    File.join(parent_directory, local)
  end
  
  def elements_qualified?; "qualified" == schema_attributes["elementFormDefault"]; end
  
  def referenced_instances
    # We do not need to include directly_contained_packages or containing packages here: 
    # if some other package is referenced, it's because of an association.
    # NOTE: we also need to include attributes here, due to enumerations.
    referenced_package_paths.collect {|path| self.class.find(path) }
  end
  
  def referenced_prefix_to_uri_hash
    answer = Hash.new
    referenced_instances.each do |inst|
      prefix = inst.namespace_prefix
      uri = inst.namespace_uri
      raise "Unable to reference #{inst.package_path} due to lack of prefix and/or uri" unless prefix && uri
      answer["xmlns:#{prefix}"]=uri
    end
    answer
  end
  
  def referenced_prefix_to_location_hash
    answer = Hash.new
    referenced_instances.each do |inst|
      prefix = inst.namespace_prefix
      raise "Unable to reference #{inst.package_path} due to lack of prefix" unless prefix
      answer[prefix]=inst.filepath
    end
    answer
  end
  
  def referenced_uri_to_location_hash
    answer = Hash.new
    referenced_instances.each do |inst|
      uri = inst.namespace_uri
      raise "Unable to reference #{inst.package_path} due to lack of uri" unless uri
      answer[uri]=inst.filepath
    end
    answer
  end
  
  # =======================================================
  #  Resolution
  #  Ensures that instances are available for all contained and referenced packages.
  # =======================================================
  
  
  def resolve
    return if resolved
    self.resolved = true
    resolve_modules
    resolve_classes
    resolve_interfaces
  end
  
  # Ensure that all directly_contained_packages recursively have corresponding instances of Schema_Builder
  def resolve_modules
    directly_contained_packages.each {|mod| self.class.instance(mod.name).resolve }
  end
  
  # Ensure that all directly_contained_classes (recursively) reference classes and interfaces in instances of Schema_Builder
  def resolve_classes
    directly_contained_classes.each {|klass| resolve_class(klass) }
    referenced_package_paths.uniq!
  end
  
  # Ensure that all directly_contained_classes (recursively) reference classes and interfaces in instances of Schema_Builder
  def resolve_interfaces
    directly_contained_interfaces.each {|interface| resolve_interface(interface) }
    referenced_package_paths.uniq!
  end
  
  def resolve_interface(interface)
    return if self.class.resolved?(interface)
    self.class.resolved_classifiers << interface
    implementors = interface.implementors
    reference_packages_containing(implementors)
  end
  
  def resolve_class(klass)
    return if self.class.resolved?(klass)
    self.class.resolved_classifiers << klass
    # puts "RESOLVING class #{klass.name}"
    raise "Encountered class (#{klass.name}) that's not JSON_Graph::Serializeable." unless klass.includes?(JSON_Graph::Serializeable)
    raise "Encountered class (#{klass.name}) that does not respond to :associations." unless klass.respond_to?(:associations)
    associations, attributes = Reference_Info.instances(klass, namespace_prefix, namespace_uri)
    # puts "JSON_Graph_State.ignored_getters_by_class[#{klass}]: #{JSON_Graph_State.ignored_getters_by_class[klass]}" if JSON_Graph_State.ignored_getters_by_class[klass]
    associations.reject!{|ref_info| JSON_Graph_State.ignored_getters_by_class[klass] && JSON_Graph_State.ignored_getters_by_class[klass].include?(ref_info.ruby_getter)}
    attributes.reject!{|ref_info| JSON_Graph_State.ignored_getters_by_class[klass] && JSON_Graph_State.ignored_getters_by_class[klass].include?(ref_info.ruby_getter)}
    assoc_referenced = associations.collect{|ref_info| ref_info.classifier}
    attr_referenced = attributes.collect{|ref_info| ref_info.fixed_classifier}
    referenced_types = assoc_referenced + attr_referenced
    sc = _find_super(klass)
    referenced_types << sc if sc # XML schema's analogy to inheritance does not suport multiple inheritance
    reference_packages_containing(referenced_types)
  end
  
  def reference_packages_containing(classifiers)
    required_container_paths = classifiers.uniq.collect do |type| 
      next if RUBY_SCHEMA_TYPE_MAP.key?(type)
      type.containing_path_or_nil 
    end
    required_container_paths.compact!
    required_container_paths.uniq!
    required_container_paths.delete package_path
    instances = required_container_paths.collect do |path| 
      referenced_package_paths << path unless referenced_package_paths.member?(path)
      self.class.instance(path, nil, nil, nil, builder_options) 
    end
    instances.each {|inst| inst.resolve }
  end
  
  def directly_contained?(klass)
    directly_contained_classes.member?(klass) || directly_contained_enumerations.member?(klass) || directly_contained_interfaces.member?(klass)
  end
  
  def find_root_reachable(klass=nil)
    return unless root_schema?
    if klass
      return unless directly_contained?(klass)
      return if root_reachable.member?(klass)
      root_reachable << klass
      return if klass.enumeration?
      associations, attributes = Reference_Info.instances(klass, namespace_prefix, namespace_uri)
      sc = _find_super(klass)
      find_root_reachable(sc) if sc
      (associations + attributes).each {|ref_info| find_root_reachable(ref_info.fixed_classifier) }
    else
      root_reachable.clear # invoked without argument only once, during resolution
      root_classes.each {|cls| find_root_reachable(cls) }
    end
  end
  
  def reachable?(classifier)
    return true unless root_schema?
    root_reachable.member?(classifier)
  end

  # =======================================================
  #  Actual schema generation code
  # =======================================================
  
  def clear; content.clear; end
  
  # Returns [prefix, local_name].
  def xml_name_parts(klass)
    answer = RUBY_SCHEMA_TYPE_MAP[klass]
    return answer if answer
    path = klass.name.split('::')
    local_name = path.pop
    pkg_path = path.join('::')
    prefix = self.class.prefix(pkg_path)
    [prefix, local_name]
  end
  
  # returns local_name, full_name  (they are the same if it's in this package)
  def xml_type_name(klass)
    prefix, local_name = xml_name_parts(klass)
    full_name = (prefix==namespace_prefix) ? local_name : "#{prefix}:#{local_name}"
    [local_name, full_name]
  end
  
  # Prepare the attributes of the <schema> element
  def expanded_schema_attributes
    expanded = schema_attributes.clone
    if namespace_prefix && namespace_uri
      expanded[:xmlns]=namespace_uri
      expanded[:targetNamespace]=namespace_uri
      expanded["xmlns:#{namespace_prefix}"]=namespace_uri
    else
      Kernel.warn "WARNING: schema specification lacks namespace_prefix and/or namespace_uri. There will be no targetNamespace."
    end
    expanded.merge! referenced_prefix_to_uri_hash
    expanded
  end


  # Makes an XML schema based on module mod, with top level elements corresponding to root_classes.
  # If a directory is specified, the schema is written to the subdirectory specified by the package path.
  def make_schema(dir=nil)
    clear
    raise "At least one unresolved Schema_Builder: #{package_path}" unless resolved
    sorted_roots = root_classes.sort {|a, b| a.name <=> b.name }
    # puts "\n\nGenerating schema for: #{package_path}. Roots are: #{sorted_roots}"
    builder.instruct! :xml, :version=>"1.0", :encoding=>"UTF-8"
    builder.comment!(top_comment) if top_comment
    builder.xsd(:schema, expanded_schema_attributes) do
      builder.comment! '========== Imports ==========' if referenced_uri_to_location_hash.any?
      referenced_uri_to_location_hash.each do |uri, location|
        next if uri == namespace_uri
        builder.xsd(:import, :namespace=>uri, :schemaLocation=>location)
      end
      builder.comment! '========== Root elements ==========' if sorted_roots.any?
      sorted_roots.each do |root_class|
        local_name, full_name = xml_type_name(root_class)
        Reference_Info.add_root(local_name, root_class, namespace_prefix, namespace_uri)
        builder.xsd(:element, :name=>local_name, :type=>full_name)
      end
      builder.comment! '========== Enumerations ==========' if directly_contained_enumerations.any?
      _gen_enumerations
      builder.comment! '========== Interfaces ==========' if directly_contained_interfaces.any?
      directly_contained_interfaces.each do |interface|
        next unless reachable?(interface)
        interface_local_name, interface_full_name = xml_type_name(interface)
        builder.xsd(:group, :name=>interface_local_name) do
          builder.xsd(:choice) do
            interface.implementors.each do |impl|
              impl_local_name, impl_full_name = xml_type_name(impl)
              # Compound name will reduce likelyhood of name conflicts (at expense of verbosity)
              ele_name = "#{interface_local_name}__#{impl_local_name}"
              builder.xsd(:element, :name => ele_name, :type => impl_full_name)
            end
          end
        end
      end
      builder.comment! '========== Classes ==========' if directly_contained_classes.any?
      directly_contained_classes.each do |klass|
        next unless reachable?(klass)
        local_name, full_name = xml_type_name(klass)
        # We need to accomodate interfaces
        # We don't know if a class is abstract
        builder.xsd(:complexType, {:name => local_name}) do
          associations, attributes = Reference_Info.instances(klass, namespace_prefix, namespace_uri)
          sc = _find_super(klass)
          if sc
            builder.xsd(:complexContent) do
              local_name, full_name = xml_type_name(sc)
              builder.xsd(:extension, :base => full_name) do
                _gen_associations(klass, associations)
                # Notice we don't generate an object_id attribute - already inherit one.
                _gen_attributes(klass, attributes)
              end
            end
          else
            _gen_associations(klass, associations)
            builder.xsd(:attribute, {:name=>"object_id", :type=>"xsd:ID", :use=>"required"}) if include_object_id
            _gen_attributes(klass, attributes)
          end
        end
      end
    end
    write(dir) if dir
    content
  end
  
  def _find_super(klass)
    sc = klass.superclass
    return sc if sc && !IGNORED_SUPERCLASSES.member?(sc.name)
    nil
  end
  
  def _gen_enumerations
    directly_contained_enumerations.each do |enumeration|
      next unless reachable?(enumeration)
      local_name, full_name = xml_type_name(enumeration)
      builder.xsd(:simpleType, {:name=>local_name}) do
        builder.xsd(:restriction, {:base=>"xsd:string"}) do
          enumeration.literal_values.each do |enum_value|
            builder.xsd(:enumeration, {:value=>enum_value})
          end
        end
      end
    end
  end
  
  def _gen_associations(klass, associations)
    unless associations.empty?
      builder.xsd(:sequence) do
        associations.each do |ref_info|
          # We lack minOccurs info
          max_occurs = (ref_info.multiplicity.max > 1) ? 'unbounded' : '1'
          local_name, full_name = xml_type_name(ref_info.classifier)
          builder.xsd(:element, {:minOccurs=>"0", :maxOccurs=>max_occurs,  :name=>ref_info.json_getter, :type=>full_name})
        end
      end
    end
  end
  
  def _gen_attributes(klass, attributes)
    attributes.each do |ref_info|
      type = ref_info.fixed_classifier 
      local_name, full_name =  xml_type_name(type)
      # We don't know if an attribute is required
      builder.xsd(:attribute, {:name=>ref_info.json_getter, :type=>full_name})
    end
  end
  
  def write(dir=nil)
    path = filepath(dir)
    dir_path = File.dirname(path)
    # We are careful to make directories because the mapping between XML schemas and UML packages
    # suggests schema file paths should map to UML package paths
    FileUtils::Verbose.mkdir_p(dir_path) unless File.exist?(dir_path)
    File.open(path, 'w') {|f| f << content }
  end

end

Schema_Builder.default_include_object_id = true