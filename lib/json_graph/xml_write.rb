require 'builder'
require 'json_graph/xml_meta_info'
require 'json_graph/time_marshalling'

class XML_Graph
  
  attr_accessor :builder, :namespace_prefix, :namespace_uri
  
  # A String used to build the output
  attr_accessor :target
  
  def namespaced?; namespace_prefix && namespace_uri; end
  
  def full_tag_name(non_namespaced_tag_name)
    return non_namespaced_tag_name unless namespaced?
    "#{namespace_prefix}:#{non_namespaced_tag_name}"
  end
  
  def self.pretty_generate(target_object, namespace_prefix=nil, namespace_uri=nil, options={});
    new.pretty_generate(target_object, namespace_prefix, namespace_uri, options)
  end
  
  def pretty_generate(target_object, namespace_prefix=nil, namespace_uri=nil, options={})
    options = options.clone # prevents side effects
    JSON_Graph_State.clear
    self.namespace_prefix = namespace_prefix
    self.namespace_uri = namespace_uri
    options[:max_nesting] ||= 1000
    options[:indent] ||= 2
    options[:target] ||= String.new
    self.target = options[:target]
    max_depth = options.delete(:max_nesting)||0
    marshalling_options = {}
    marshalling_options[:allow_duplicates] = options.delete(:allow_duplicates) || false
    self.builder = Builder::XmlMarkup.new(options)
    json = target_object.to_marshalling_form(max_depth, 0, marshalling_options)
    # require 'pp'; pp json
    json._marshal_xml(self, target_object.class.name.split("::").last.to_sym)
    # puts "#####################################"
    # puts xml
    # puts "#####################################"
    target
  end
  
  def element(name, attrib_hash = {}, &child_block)
    if target.empty?
      builder.instruct! :xml, :version=>"1.0", :encoding=>"UTF-8"
      attrib_hash[:xmlns] = namespace_uri if namespace_uri
      attrib_hash[:"xmlns:#{namespace_prefix}"] = namespace_uri if namespaced?
    end
    attrib_hash = attrib_hash.keep_if {|name, value| value }
    # puts "\n#{name.inspect}  #{attrib_hash.inspect}"
    builder.__send__(full_tag_name(name).to_sym, attrib_hash, &child_block)
  end
  
  alias xml target
  alias xml= target=
  
end


class Object
  def to_marshalling_form(max_depth=1, current_depth=0, marshalling_options = {}); raise "Unexpected receiver (#{self.class}) for to_marshalling_form. Either adjust which attributes are traversed, or mix in JSON_Graph::Serializeable."; end
  def _marshal_xml(marshaller, role); raise "Unexpected receiver (#{self.class}) for _marshal_xml. Either adjust which attributes are traversed, or mix in JSON_Graph::Serializeable."; end
  def _to_xml; raise "Unexpected receiver (#{self.class}) for _to_xml. Either adjust which attributes are traversed, or mix in JSON_Graph::Serializeable."; end
  def _xml_attribute?; raise "Unexpected receiver (#{self.class}) for _xml_attribute?. Either adjust which attributes are traversed, or mix in JSON_Graph::Serializeable."; end
end

module Simple_Serializeable
  def to_marshalling_form(max_depth=1, current_depth=0, marshalling_options = {})
    raise "Called to_marshalling_form with arguments that do not allow for a single execution" if 0!=max_depth && current_depth >= max_depth
    return self
  end
  def _to_xml; to_s; end
  def _xml_attribute?; true; end
  # This happens only when simple things are included in an Array
  def _marshal_xml(marshaller, role)
    full_name = marshaller.full_tag_name(role)
    marshaller.builder.__send__(full_name.to_sym, to_s)
  end
end

module Boolean
  include Simple_Serializeable  
end

class TrueClass
  include Boolean
  # This is more lax than the W3C specification, which only allows '1', 'true', '0', 'false'
  # NOTE: This is implemented in TrueClass because that's what SSL uses as the type (in order
  # to be compatible with Sequel)
  def self._from_xml(str)
    value = __from_xml(str)
    # puts "=======> String #{str.inspect} was decoded into #{value.inspect}"
    value
  end
  def self.__from_xml(str)
    case str
      when '1', 'true', 'True', 'TRUE', 't', 'T' ; true
      when '0', 'false', 'False', 'FALSE', 'f', 'F' ; false
      when nil, '' ; nil
      else raise "Unexpected value for Boolean: #{self.inspect}"
    end
  end
end
class FalseClass
  include Boolean
end

class String
  include Simple_Serializeable
  def self._from_xml(str); str; end
end

class NilClass
  include Simple_Serializeable
  def _to_xml; self; end
end

class Numeric
  include Simple_Serializeable
  # The sublesses of Numeric (indirect as well as direct) are:  [Integer, Fixnum, Float, Bignum, Rational, Complex]
end

class Integer
  def self._from_xml(str); str.to_i; end
end

class Float
  def self._from_xml(str); str.to_f; end
end

class DateTime
  include Simple_Serializeable
  def _to_xml; to_s_with_nsec; end
  def self._from_xml(str); from_s_with_nsec(str); end
end

class Time
  include Simple_Serializeable
  def _to_xml; to_s_with_nsec; end
  def self._from_xml(str); from_s_with_nsec(str); end
end

class Date
  include Simple_Serializeable
  def _to_xml; to_s; end
  def self._from_xml(str); Date.parse(str); end
end

class Symbol
  def to_marshalling_form(max_depth=1, current_depth=0, marshalling_options = {})
    to_s.to_marshalling_form(max_depth, current_depth, marshalling_options)
  end
  def _to_xml; to_s; end
end

class Array
  def to_marshalling_form(max_depth=1, current_depth=0, marshalling_options = {})
    current_depth += 1
    raise "Called to_marshalling_form with arguments that do not allow for a single execution" if 0!=max_depth && current_depth > max_depth
    return [] unless 0==max_depth || current_depth < max_depth
    collect {|element| element.to_marshalling_form(max_depth, current_depth, marshalling_options) }
  end
  def _xml_attribute?; false; end
  def _marshal_xml(marshaller, role)
    builder = marshaller.builder
    each {|kid| kid._marshal_xml(marshaller, role) }
  end
end

class Hash
  # See comment on version in Serializabe. 
  # This version is intended to represent a Ruby Hash (Smalltalk Dictionary, Java Map, Perl associative array) instead of an object.
  # setting max_depth to 0 indicates that depth is infinite
  def to_marshalling_form(max_depth=1, current_depth=0, marshalling_options = {})
    current_depth += 1
    raise "Called to_marshalling_form with arguments that do not allow for a single execution" if 0!=max_depth && current_depth > max_depth
    entries = Array.new
    if 0==max_depth || current_depth < max_depth
      each {|key, value|
        key = key.to_marshalling_form(max_depth, current_depth,marshalling_options)
        value = value.to_marshalling_form(max_depth, current_depth, marshalling_options)
        hash_rep = JSON_Graph_State.classname? ? { JSON_Graph_State.classname_key => 'Entry' } : {}
        hash_rep = hash_rep.merge({ 'key' => key, 'value' => value })
        entries << hash_rep
        }
    end
    JSON_Graph_State.classname? ? {JSON_Graph_State.classname_key => 'Dictionary', 'entries' => entries} : {'entries' => entries}
  end
  def _xml_attribute?; false; end
  def _partition
    attributes = Hash.new
    associations = Hash.new
    each {|key, value|
      if value._xml_attribute?
        attributes[key] = value._to_xml
      else
        associations[key] = value
      end
      }
    [attributes, associations]
  end
  def _marshal_xml(marshaller, role)
    klone = clone
    class_name = klone.delete(JSON_Graph_State.classname_key)
    ref_key = JSON_Graph_State.references? ? klone.delete(JSON_Graph_State.ref_key) : nil
    builder = marshaller.builder
    case
      when ref_key
        # Reference to a previously marshalled complex object
        raise "Expected reference contain only class name and ref key, but found: #{self.inspect}" unless klone.empty?
        marshaller.element(:Reference, :classname => class_name, :idref => ref_key)
      when 'Dictionary'==class_name
        # Hash represents a Hash/Dictionary/Map/AssociativeArray
        marshaller.element(:Dictionary) {
          entries = klone['entries']
          entries.each {|entry| 
            key = entry['key']
            value = entry['value']
            marshaller.element(:Element) {
              if key._xml_attribute?
                key_attrs = {:value => key}
                marshaller.element(:Key, key_attrs)
              else
                key._marshal_xml(marshaller, :Key)
              end
              if value._xml_attribute?
                value_attrs = {:value => key}
                marshaller.element(:Value, value_attrs)
              else
                key._marshal_xml(marshaller, :Value)
              end
            } # Element
          } # entries
        } # Dictionary
      else
        # Hash represents a complex object
        attributes, associations = _partition
        attributes.delete(JSON_Graph_State.id_key) unless JSON_Graph_State.references?
        attributes.delete(JSON_Graph_State.classname_key) unless JSON_Graph_State.classname?
        if associations.empty?
          marshaller.element(role, attributes)
        else
          marshaller.element(role, attributes) {
            associations.each {|key, value| value._marshal_xml(marshaller, key) }
            }
        end
    end
  end
end
