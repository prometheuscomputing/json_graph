require_relative 'spec_helper'
require 'json_graph'
require 'json_graph/xml_support'
require 'json_graph/xml_schema_builder'
require 'model_metadata'
require 'date'

require 'pp'

module Kooky
  
  class Family
    include JSON_Graph::Serializeable
    extend ModelMetadata
    attr_accessor :last_name, :members
    def initialize(last_name)
      self.last_name = last_name
      self.members = Array.new
    end
    def << (member); member.family = self; members << member; end
    # SSA Methods, created manually so we can test marshalling of non-SSA objects
    def self.parents; []; end
    def self.interfaces; []; end
    def self.attributes; { :last_name => {:class => 'String'}}; end
    def self.associations; { :members => {:class => 'Kooky::Person', :type => :many_to_many} }; end
  end

  class Person
    include JSON_Graph::Serializeable
    extend ModelMetadata
    attr_accessor :first_name, :role, :pets, :spouse, :siblings, :children, :family, :num_fingers, :is_bald, :birthday, :favorite_float
    def initialize(first_name, role)
      self.first_name = first_name
      self.role = role
      self.pets = Array.new
      self.siblings = Array.new
      self.children = Array.new
    end
    
    # this will barf if family is nil...
    def full_name
      "#{first_name} #{family.last_name}"
    end
    # We don't really need the setter, because the getter is derived.
    # Of course we could make this a no-op, or we could split the full name, and set both first_name and family.last_name
    # We instead stash the value in @redundant_full_name so that we can verify when :full_name is ignored.
    # If we do not ignore full_name, unmarshalling will attempt to set this value
    def full_name=(val); @redundant_full_name=val; end
    def redundant_full_name; @redundant_full_name; end
    # SSA Methods, created manually so we can test marshalling of non-SSA objects
    def self.parents; []; end
    def self.interfaces; []; end
    def self.attributes; { :first_name => {:class => 'String'}, :role => {:class => 'String'}, :full_name => {:class => 'String'}, :num_fingers => {:class => 'Integer'}, :is_bald => {:class => 'TrueClass'}, :birthday => {:class => 'DateTime'}, :favorite_float => {:class => 'Float'}}; end
    def self.associations; { :pets => {:class => 'Kooky::Pet', :type => :many_to_many}, :siblings => {:class => 'Kooky::Person', :type => :many_to_many}, :children => {:class => 'Kooky::Person', :type => :many_to_many}, :spouse => {:class => 'Kooky::Person'}, :family => {:class => 'Kooky::Family'} }; end
  end
  
  class Pet
    include JSON_Graph::Serializeable
    extend ModelMetadata
    attr_accessor :name, :type
    def initialize(name, type)
      self.name = name
      self.type = type
    end
    # SSA Methods, created manually so we can test marshalling of non-SSA objects
    def self.parents; []; end
    def self.interfaces; []; end
    def self.attributes; { :name => {:class => 'String'}, :type => {:class => 'String'} }; end
    def self.associations; {}; end
  end
end # module Kooky

include Kooky

MARSHALING_SPECIFICATION = {
  Family => 
      {
        :last_name => nil,
        :members => nil
      },

  Person =>
      {
        :full_name => nil,   # Derived accessor
        :first_name => nil,
        :role => nil,
        :pets => nil,
        :spouse => nil,
        :children => nil,
        :is_bald => nil,
        :num_fingers =>  nil,
        :birthday => nil,
        :favorite_float => nil
      },
  
  Pet =>
      {
        :name => nil,
        :type => nil
      }
}

family = Family.new('Addams')
gomez = Person.new('Gomez', 'Dad')
morticia = Person.new('Morticia', 'Mom')
fester = Person.new('Fester', 'Uncle')
lurch = Person.new('Lurch', 'Butler')
granny = Person.new('Granny Frump', 'Grandmama')
pugsley = Person.new('Pugsley', 'Son')
wednesday = Person.new('Wednesday', 'Daughter')
itt = Person.new('Itt', 'Cousin')
thing = Person.new('Thing', 'Thing')

gomez.spouse = morticia
morticia.spouse = gomez
wednesday.siblings << pugsley
pugsley.siblings << wednesday
morticia.children << wednesday << pugsley
gomez.children << pugsley << wednesday
andy = Person.new('Andy', 'Sheriff')

family << gomez
family << morticia
family << fester
family << lurch
family << granny
family << pugsley
family << wednesday
family << itt
family << thing


gomez.num_fingers=12
fester.is_bald=true
fester.favorite_float=3.1
itt.is_bald=false
thing.birthday=DateTime.parse("2015-07-30 13:53:50:000000000 +0000")

gomez.pets << Pet.new('Kitty Cat', 'Lion') << Pet.new('Zelda', 'Vulture')
pugsley.pets << Pet.new('Aristotle', 'Octopus')
wednesday.pets << Pet.new('Homer', 'Spider')
morticia.pets << Pet.new('Cleopatra', 'African Strangler Vine') << Pet.new('Bernice', 'Giant Squid')
andy.pets << Pet.new('Arnold', 'Pig') # OK, so Arnold was really from Green Acres...sue me.

def verify_graph(root_obj, with_refs, ignored_full_name)
  root_obj.should be_instance_of(Family)
  root_obj.last_name.should == 'Addams'
  members = root_obj.members
  members.should be_instance_of(Array)
  gomez = members.first
  gomez.should be_instance_of(Person)
  gomez.first_name.should == 'Gomez'
  gomez.role.should == 'Dad'
  gomez.redundant_full_name.should be_nil if ignored_full_name
  gomez_pets = gomez.pets
  gomez_pets.size.should == 2
  gomez_pets.first.name.should == 'Kitty Cat'
  gomez_pets.first.type.should == 'Lion'
  gomez_pets.last.name.should == 'Zelda'
  gomez_pets.last.type.should == 'Vulture'
  morticia = members[1]
  morticia.should be_instance_of(Person)
  morticia.first_name.should == 'Morticia'
  morticia.role.should == 'Mom'
  morticia_pets = morticia.pets
  morticia_pets.size.should == 2
  morticia_pets.first.name.should == 'Cleopatra'
  morticia_pets.first.type.should == 'African Strangler Vine'
  morticia_pets.last.name.should == 'Bernice'
  morticia_pets.last.type.should == 'Giant Squid'
  pugsley = members[5]
  pugsley.should be_instance_of(Person)
  pugsley.first_name.should == 'Pugsley'
  pugsley.role.should == 'Son'
  pugsley.pets.size.should == 1
  pugsley.pets.first.name.should == 'Aristotle'
  pugsley.pets.first.type.should == 'Octopus'
  wednesday = members[6]
  wednesday.should be_instance_of(Person)
  wednesday.first_name.should == 'Wednesday'
  wednesday.role.should == 'Daughter'
  wednesday.pets.size.should == 1
  wednesday.pets.first.name.should == 'Homer'
  wednesday.pets.first.type.should == 'Spider'
  return unless with_refs
  gomez.spouse.should be_equal(morticia)
  morticia.spouse.should be_equal(gomez)
  morticia.children.first.should be_equal(wednesday)
  morticia.children.last.should be_equal(pugsley)
  gomez.children.first.should be_equal(pugsley)
  gomez.children.last.should be_equal(wednesday)
  # The following are nil because :siblings is not included in MARSHALING_SPECIFICATION. 
  # Siblings are present in the original objects, but not in JSON or XML files or in unmarshalled objects.
  wednesday.siblings.should be_nil
  pugsley.siblings.should be_nil
end

describe JSON_Graph do
  
  it "should serialize an object graph to JSON" do
    actual = JSON_Graph.pretty_generate(family, OPTIONS)
    compare('addams_withRef.json', actual)
  end
  
  it "should serialize an object graph to JSON (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    actual = JSON_Graph.pretty_generate(family, OPTIONS)
    filename = 'addams_withRef.json'
    compare(filename, actual)
  end
  
  it "should serialize a tree to JSON (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    JSON_Graph_State.no_references!
    actual = JSON_Graph.pretty_generate(family, OPTIONS)
    filename = 'addams_withoutRef.json'
    compare(filename, actual)
  end
  
  it "should deserialize JSON to an object graph to JSON (using json_getter_overrides_by_class & ignored_getters)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :full_name
    filename = 'addams_withRef.json'
    path = filename.fileName_to_expected_path
    root_obj = path.path2obj
    verify_graph(root_obj, true, true)
  end
  
  # This is identical to above except for not ignoring full_name
  it "should deserialize JSON to an object graph to JSON (using json_getter_overrides_by_class without ignored_getters)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    # JSON_Graph_State.ignored_getters << :full_name
    filename = 'addams_withRef.json'
    path = filename.fileName_to_expected_path
    root_obj = path.path2obj
    verify_graph(root_obj, true, false)
  end

end

describe Object do
  
  it "when Serializable is mixed in, getter_mapping and ruby_getters are added on the class side" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    Family.getter_mapping.should ==  MARSHALING_SPECIFICATION[Family].to_mapping
    Person.getter_mapping.should ==  MARSHALING_SPECIFICATION[Person].to_mapping
    Pet.getter_mapping.should ==  MARSHALING_SPECIFICATION[Pet].to_mapping
    Family.ruby_getters.should == [:last_name, :members]
    Person.ruby_getters.should ==  [ :full_name, :first_name, :role, :pets, :spouse, :children, :is_bald, :num_fingers, :birthday, :favorite_float ]
    Pet.ruby_getters.should ==  [:name, :type]
  end
  
  it "when Serializable is mixed in, getter_mapping and ruby_getters are added on the class side (mapping does not contain ignored_getters)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    Family.getter_mapping.should ==  MARSHALING_SPECIFICATION[Family].to_mapping
    Person.getter_mapping.should ==  {
        :full_name => nil,
        :first_name => nil,
        :role => nil,
        :pets => nil,
        :is_bald=>:is_bald, 
        :num_fingers=>:num_fingers, 
        :birthday=>:birthday,
        :favorite_float => :favorite_float,
      }.to_mapping
    Pet.getter_mapping.should ==  MARSHALING_SPECIFICATION[Pet].to_mapping
    Family.ruby_getters.should == [:last_name, :members]
    Person.ruby_getters.should ==  [ :full_name, :first_name, :role, :pets, :is_bald, :num_fingers, :birthday, :favorite_float ]
    Pet.ruby_getters.should ==  [:name, :type]
  end
  
  it "when Serializable is mixed in, getter_mapping and ruby_getters are added on the class side (mapping does not contain getters specified in ignored_getters_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters_by_class[Person] = [:first_name, :role]
    Family.getter_mapping.should ==  MARSHALING_SPECIFICATION[Family].to_mapping
    Person.getter_mapping.should ==  {
        :full_name => nil,
        :pets => nil,
        :spouse => nil,
        :children => nil,
        :birthday => :birthday,
        :is_bald => :is_bald,
        :num_fingers => :num_fingers,
        :favorite_float=>:favorite_float
      }.to_mapping
    Pet.getter_mapping.should ==  MARSHALING_SPECIFICATION[Pet].to_mapping
    Family.ruby_getters.should == [:last_name, :members]
    Person.ruby_getters.should ==  [ :full_name, :pets, :spouse, :children, :is_bald, :num_fingers, :birthday, :favorite_float ]
    Pet.ruby_getters.should ==  [:name, :type]
  end
  
end

describe XML_Graph do
  
  it "should serialize an object graph to XML, without namespaces (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    actual = XML_Graph.pretty_generate(family, nil, nil, OPTIONS)
    filename = 'addams_withRef.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, without namespaces (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, nil, nil, OPTIONS)
    filename = 'addams_withoutRef.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, without types, without namespaces (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << JSON_Graph_State.classname_key
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, nil, nil, OPTIONS)
    filename = 'addams_withoutRef_withoutType.xml'
    compare(filename, actual)
  end
  
  ###########################
  
  it "should serialize an object graph to XML, with namespaces (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    actual = XML_Graph.pretty_generate(family, 'pro', 'urn:prometheuscomputing.com/Kooky', OPTIONS)
    filename = 'addams_withRef_namespaced.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, with namespaces (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, 'pro', 'urn:prometheuscomputing.com/Kooky', OPTIONS)
    filename = 'addams_withoutRef_namespaced.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, without types, with namespaces (using json_getter_overrides_by_class)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << JSON_Graph_State.classname_key
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, 'pro', 'urn:prometheuscomputing.com/Kooky', OPTIONS)
    filename = 'addams_withoutRef_withoutType_namespaced.xml'
    compare(filename, actual)
  end

end

describe JSON_Graph do
  
  # MODIFY THE DOMAIN CLASSES by adding class-side json_getter_overrides
  MARSHALING_SPECIFICATION.each {|klass, overrides|
    command = "def self.json_getter_overrides; #{overrides.inspect}; end"
    klass.module_eval(command)
  }
  
  it "should serialize an object graph to JSON (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    actual = JSON_Graph.pretty_generate(family, OPTIONS)
    filename = 'addams_withRef.json'
    compare(filename, actual)
  end
  
  it "should serialize a tree to JSON (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    JSON_Graph_State.no_references!
    actual = JSON_Graph.pretty_generate(family, OPTIONS)
    filename = 'addams_withoutRef.json'
    compare(filename, actual)
  end

end

describe XML_Graph do
  
  it "should serialize an object graph to XML, without namespaces (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    actual = XML_Graph.pretty_generate(family, nil, nil, OPTIONS)
    filename = 'addams_withRef.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, without namespaces (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, nil, nil, OPTIONS)
    filename = 'addams_withoutRef.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, without types, without namespaces (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << JSON_Graph_State.classname_key
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, nil, nil, OPTIONS)
    filename = 'addams_withoutRef_withoutType.xml'
    compare(filename, actual)
  end
  
  ###########################
  
  it "should serialize an object graph to XML, with namespaces (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    actual = XML_Graph.pretty_generate(family, 'pro', 'urn:prometheuscomputing.com/Kooky', OPTIONS)
    filename = 'addams_withRef_namespaced.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, with namespaces (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << :family
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, 'pro', 'urn:prometheuscomputing.com/Kooky', OPTIONS)
    filename = 'addams_withoutRef_namespaced.xml'
    compare(filename, actual)
  end
  
  it "should serialize a tree to XML, without types, with namespaces (using class-side json_getter_overrides)" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.ignored_getters << :siblings << :spouse << :children << JSON_Graph_State.classname_key
    JSON_Graph_State.no_references!
    actual = XML_Graph.pretty_generate(family, 'pro', 'urn:prometheuscomputing.com/Kooky', OPTIONS)
    filename = 'addams_withoutRef_withoutType_namespaced.xml'
    compare(filename, actual)
  end

  # NOTE:  This hash could be transformed directly to JSON without going through intermediate objects
  it "Parses XML to Hash" do
    JSON_Graph_State.clear_config
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
    JSON_Graph_State.no_references!
    
    Reference_Info.clear
    # Remove any existing Schema_Builder instances
    Schema_Builder.clear
    # Specify which Strings are actually Enumerations (must be done before instance creation)
    Schema_Builder.type_override = {
    }
    Schema_Builder.root_classes = [Family]
    # We make the schemas only to set up the Reference_Info instances to enable parsing.
    Schema_Builder.make_schemas

    # There is currently a problem with writing references: the role is not included.
    # instance_path = 'addams_withRef_namespaced.xml'.fileName_to_expected_path
    # There is currently a problem with writing types.  The "type" attribute is likely to result in collisions
    # instance_path = 'addams_withoutRef_namespaced.xml'.fileName_to_expected_path
    instance_path = 'addams_withoutRef_withoutType_namespaced.xml'.fileName_to_expected_path
    root = XML_Graph.path2hash(instance_path)
    root['last_name'].should == 'Addams'
    members = root[:members] # Associations currently keyed by symbols, attributes by string. That's not intentional.
    members.should be_instance_of(Array)
    members.size.should == 9
    gomez   = members[0]
    fester  = members[2]
    itt     = members[7]
    thing   = members[8]
    gomez["@type"].should == 'Kooky::Person'
    gomez['num_fingers'].should == 12
    fester["@type"].should == 'Kooky::Person'
    fester['is_bald'].should be true
    fester['favorite_float'].should == 3.1
    itt["@type"].should =='Kooky::Person'
    itt['is_bald'].should be false
    thing["@type"].should == 'Kooky::Person'
    thing['birthday'].should == DateTime.parse("2015-07-30 13:53:50:000000000 +0000")
    
  end
  
  it "Parses XML to Object" do
    # This uses the same setup as "Parses XML to Hash"
    instance_path = 'addams_withoutRef_withoutType_namespaced.xml'.fileName_to_expected_path
    root = XML_Graph.path2obj(instance_path)
    # pp root
    root.should be_instance_of(Kooky::Family)
    root.last_name.should == 'Addams'
    members = root.members
    members.should be_instance_of(Array)
    members.size.should == 9
    gomez   = members[0]
    fester  = members[2]
    itt     = members[7]
    thing   = members[8]
    gomez.should be_instance_of(Kooky::Person)
    gomez.num_fingers.should == 12
    fester.should be_instance_of(Kooky::Person)
    fester.is_bald.should be true
    fester.favorite_float.should == 3.1
    itt.should be_instance_of(Kooky::Person)
    itt.is_bald.should be false
    thing.should be_instance_of(Kooky::Person)
    thing.birthday.should == DateTime.parse("2015-07-30 13:53:50:000000000 +0000")
  end
  
  it "Parses Date, Time, DateTime objects correctly" do
    # Test DateTime conversion to/from xml
    dt = DateTime.now
    dt.should == DateTime._from_xml(dt._to_xml)
    
    # Test Time conversion to/from xml
    t = Time.now
    t.should == Time._from_xml(t._to_xml)
    
    # Test Date conversion to/from xml
    d = Date.today
    d.should == Date._from_xml(d._to_xml)
  end
end

# NOTE: This test appears to be incomplete. There is no definition for 'usage_proc'.
#       I'm disabling it for now. -SD
# describe XML_Graph do
#   it "should properly handle rules applied to getters" do
#     JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION.clone
#     JSON_Graph_State.ignored_getters.clear
#     JSON_Graph_State.ignored_getters_by_class.clear
#     JSON_Graph_State.std_references!
#     JSON_Graph_State.getter_tests << usage_proc
#     actual = XML_Graph.pretty_generate(andy, nil, nil, OPTIONS)
#     puts actual
#     filename = 'andy.xml'
#     compare(filename, actual)
#   end
# end