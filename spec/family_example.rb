module Kooky
  
  class Family
    include JSON_Graph::Serializeable
    extend ModelMetadata
    attr_accessor :last_name, :members
    def initialize(last_name)
      self.last_name = last_name
      self.members = Array.new
    end
    def << (member); member.family = self; members << member; end
    # SSA Methods, created manually so we can test marshalling of non-SSA objects
    def self.parents; []; end
    def self.interfaces; []; end
    def self.attributes; { :last_name => {:class => 'String'}}; end
    def self.associations; { :members => {:class => 'Kooky::Person', :type => :many_to_many} }; end
  end

  class Person
    include JSON_Graph::Serializeable
    extend ModelMetadata
    attr_accessor :first_name, :role, :pets, :spouse, :siblings, :children, :family, :num_fingers, :is_bald, :birthday, :favorite_float
    def initialize(first_name, role)
      self.first_name = first_name
      self.role = role
      self.pets = Array.new
      self.siblings = Array.new
      self.children = Array.new
    end
    
    # this will barf if family is nil...
    def full_name
      "#{first_name} #{family.last_name}"
    end
    # We don't really need the setter, because the getter is derived.
    # Of course we could make this a no-op, or we could split the full name, and set both first_name and family.last_name
    # We instead stash the value in @redundant_full_name so that we can verify when :full_name is ignored.
    # If we do not ignore full_name, unmarshalling will attempt to set this value
    def full_name=(val); @redundant_full_name=val; end
    def redundant_full_name; @redundant_full_name; end
    # SSA Methods, created manually so we can test marshalling of non-SSA objects
    def self.parents; []; end
    def self.interfaces; []; end
    def self.attributes; { :first_name => {:class => 'String'}, :role => {:class => 'String'}, :full_name => {:class => 'String'}, :num_fingers => {:class => 'Integer'}, :is_bald => {:class => 'TrueClass'}, :birthday => {:class => 'DateTime'}, :favorite_float => {:class => 'Float'}}; end
    def self.associations; { :pets => {:class => 'Kooky::Pet', :type => :many_to_many}, :siblings => {:class => 'Kooky::Person', :type => :many_to_many}, :children => {:class => 'Kooky::Person', :type => :many_to_many}, :spouse => {:class => 'Kooky::Person'}, :family => {:class => 'Kooky::Family'} }; end
  end
  
  class Pet
    include JSON_Graph::Serializeable
    extend ModelMetadata
    attr_accessor :name, :type
    def initialize(name, type)
      self.name = name
      self.type = type
    end
    # SSA Methods, created manually so we can test marshalling of non-SSA objects
    def self.parents; []; end
    def self.interfaces; []; end
    def self.attributes; { :name => {:class => 'String'}, :type => {:class => 'String'} }; end
    def self.associations; {}; end
  end
end # module Kooky

include Kooky

MARSHALING_SPECIFICATION = {
  Family => 
      {
        :last_name => nil,
        :members => nil
      },

  Person =>
      {
        :full_name => nil,   # Derived accessor
        :first_name => nil,
        :role => nil,
        :pets => nil,
        :spouse => nil,
        :children => nil,
        :is_bald => nil,
        :num_fingers =>  nil,
        :birthday => nil,
        :favorite_float => nil
      },
  
  Pet =>
      {
        :name => nil,
        :type => nil
      }
}