require_relative 'spec_helper'
require 'json_graph'
require 'json_graph/xml_schema_builder'

# These methods are used during testing, in place of SSA method.
# These methods should not be used for production.
class Module
  def interface?; @is_interface||=false; end
  def is_interface=(boolean); @is_interface||=false; end
end

module Unknown; end
module Piggy; end
Enumeration.create :'Piggy::HousingMaterial', 'Straw', 'Sticks', 'Bricks', 'Titanium'
Enumeration.create :Marsupials, 'Kangaroo', 'Koala', 'Duck-billed platypus'
Enumeration.create :'Unknown::EyeColor', 'Fuligin', 'Octarine', 'Smaudre', 'Rawn', 'Pallow'

module TestJunk
  module Toasted
    class Cheese
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; []; end
      def self.attributes; {}; end
      def self.associations; {}; end
    end
  end
  module FiddleFaddle
    class SomeInterface 
      is_interface=true  # SSA uses classes to represent interfaces
      def self.interface?; true; end
      def self.implementors; [TestJunk::Bazinga::SoftKitty, TestJunk::Foo::Bif::Bang::Snap]; end
    end
  end
  module Bazinga
    class SoftKitty
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; [TestJunk::FiddleFaddle::SomeInterface]; end 
      def self.attributes; { :eye_color => {:class => 'String'}}; end
      def self.associations; {}; end
    end
  end
  module Foo
    class Bar
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; []; end
      def self.attributes; {}; end
      def self.associations; {}; end
    end
    class Baz
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; []; end
      def self.attributes; {}; end
      def self.associations; {}; end
    end
    module Bif
      module Bang
        class Snap
          include JSON_Graph::Serializeable
          def self.parents; []; end
          def self.interfaces; [TestJunk::FiddleFaddle::SomeInterface]; end
          def self.attributes; {}; end
          def self.associations; {}; end
        end
        class Crackle < TestJunk::Toasted::Cheese
          include JSON_Graph::Serializeable
          def self.parents; [TestJunk::Toasted::Cheese]; end
          def self.interfaces; []; end
          def self.attributes; { :material => {:class => 'String'} }; end
          def self.associations; { :rubbish => {:class => 'TestJunk::Bazinga::SoftKitty'}, :junque => {:class => 'TestJunk::FiddleFaddle::SomeInterface'} }; end
        end
      end
      module Boom
        class Pop
          include JSON_Graph::Serializeable
          def self.parents; []; end
          def self.interfaces; []; end
          def self.attributes; {}; end
          def self.associations; {}; end
        end
      end
    end
  end
end

# This omits SoftKitty.eye_color
MARSHALING_SPECIFICATION_2 = {
  TestJunk::Foo::Bif::Bang::Crackle => 
      {
        :rubbish => nil,
        :junque => nil,
        :material => nil
      }
}

MARSHALING_SPECIFICATION_3 = {
  TestJunk::Bazinga::SoftKitty => { :eye_color => nil},
  TestJunk::Foo::Bif::Bang::Crackle => 
      {
        :rubbish => nil,
        :junque => nil,
        :material => nil
      }
}

describe Schema_Builder do
  
  it "should know instances" do
    Schema_Builder.clear
    inst = Schema_Builder.new('TestJunk::Foo')
    Schema_Builder.instance('TestJunk::Foo').should equal(inst)
  end
  
  it "should find package path's module" do
    Schema_Builder.clear
    Schema_Builder.new('TestJunk::Foo').mod.should == TestJunk::Foo
  end
  
  it "should find package path's directly contained classes" do
    Schema_Builder.clear
    Schema_Builder.new('TestJunk::Foo').directly_contained_classes.should == [TestJunk::Foo::Bar, TestJunk::Foo::Baz]
  end
  
  it "should find package path's directly contained modules" do
    Schema_Builder.clear
    Schema_Builder.new('TestJunk::Foo').directly_contained_packages.should == [TestJunk::Foo::Bif]
  end
  
  it "should guess a namespace_prefix if not provided one" do
    Schema_Builder.clear
    Schema_Builder.new('TestJunk::Foo').namespace_prefix.should == 'TestJunk_Foo'
  end
  
  it "should guess a namespace_uri if not provided one" do
    Schema_Builder.clear
    Schema_Builder.new('TestJunk::Foo').namespace_uri.should == 'urn:prometheuscomputing.com/TestJunk_Foo'
  end
  
  it "creates one instance at a time (additional instances may be created by #resolve)" do
    Schema_Builder.clear
    Schema_Builder.new('TestJunk::Foo')
    Schema_Builder.all_instances.size.should == 1
  end
  
  it "resolve recursively builds instances for directly and inderictly contained modules, and modules containing referenced classifiers" do
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION_2.clone
    Schema_Builder.clear
    Schema_Builder.all_instances.size.should == 0
    # Specify which Strings are actually Enumerations (must be done before instance creation)
    Schema_Builder.type_override = {
      [TestJunk::Bazinga::SoftKitty, :eye_color] => Unknown::EyeColor,
      [TestJunk::Foo::Bif::Bang::Crackle, :material] => Piggy::HousingMaterial
    }
    instance = Schema_Builder.new('TestJunk::Foo')
    instance.resolve
    Schema_Builder.find('TestJunk::Foo').should_not == nil
    Schema_Builder.find('TestJunk::Foo::Bif').should_not == nil
    Schema_Builder.find('TestJunk::Foo::Bif::Bang').should_not == nil
    Schema_Builder.find('TestJunk::Foo::Bif::Boom').should_not == nil
    Schema_Builder.find('TestJunk::Bazinga').should_not == nil #  Included because it's referenced by Crackle#rubbish
    Schema_Builder.find('TestJunk::FiddleFaddle').should_not == nil #  Included because it's referenced by Crackle#junque
    bang_path = 'TestJunk::Foo::Bif::Bang'
    bang = Schema_Builder.instance(bang_path)
    bang.should_not == nil
    bang.package_path.should == bang_path
    bang.directly_contained_classes.should == [TestJunk::Foo::Bif::Bang::Snap, TestJunk::Foo::Bif::Bang::Crackle]
    package_paths = Schema_Builder.all_instances.collect {|inst| inst.package_path }.sort
    package_paths.should == ["Piggy", "TestJunk::Bazinga", "TestJunk::FiddleFaddle", "TestJunk::Foo", "TestJunk::Foo::Bif", "TestJunk::Foo::Bif::Bang", "TestJunk::Foo::Bif::Boom", "TestJunk::Toasted"]
  end
  
  it "finds XML names for classes" do
    # This relies on instances set up by "resolve recursively builds instances..."
    instance = Schema_Builder.instance('TestJunk::Foo::Bif')
    instance.xml_name_parts(TestJunk::Bazinga::SoftKitty).should == ['TestJunk_Bazinga', 'SoftKitty']
    instance.xml_name_parts(TestJunk::FiddleFaddle::SomeInterface).should == ['TestJunk_FiddleFaddle', 'SomeInterface']
    instance.xml_name_parts(TestJunk::Foo::Bar).should == ['TestJunk_Foo', 'Bar']
  end
  
  it "knows referenced instances" do
    # This relies on instances set up by "resolve recursively builds instances..."
    bang = Schema_Builder.find('TestJunk::Foo::Bif::Bang')
    piggy = Schema_Builder.find('Piggy')
    bazinga = Schema_Builder.find('TestJunk::Bazinga')
    fiddlefaddle = Schema_Builder.find('TestJunk::FiddleFaddle')
    toasted = Schema_Builder.find('TestJunk::Toasted')
    bang.referenced_instances.should == [bazinga, fiddlefaddle, piggy, toasted]  # The order is not important
  end
  
  it "finds prefix and uri of referenced schemas" do
    # This relies on instances set up by "resolve recursively builds instances..."
    bang = Schema_Builder.instance('TestJunk::Foo::Bif::Bang')
    bang.referenced_prefix_to_uri_hash.should == {
      "xmlns:TestJunk_Bazinga"=>"urn:prometheuscomputing.com/TestJunk_Bazinga", 
      "xmlns:TestJunk_FiddleFaddle"=>"urn:prometheuscomputing.com/TestJunk_FiddleFaddle",
      "xmlns:Piggy"=>"urn:prometheuscomputing.com/Piggy",
      "xmlns:TestJunk_Toasted"=>"urn:prometheuscomputing.com/TestJunk_Toasted"
      }
  end
  
  it "finds prefix and location of referenced schemas" do
    # This relies on instances set up by "resolve recursively builds instances..."
    bang = Schema_Builder.instance('TestJunk::Foo::Bif::Bang')
    bang.referenced_prefix_to_location_hash.should == {
      "TestJunk_Bazinga"=>"TestJunk/Bazinga.xsd", 
      "TestJunk_FiddleFaddle"=>"TestJunk/FiddleFaddle.xsd",
      "Piggy"=>"Piggy.xsd", 
      "TestJunk_Toasted"=>"TestJunk/Toasted.xsd"
      }
  end
  
  it "computes schema attributes" do
    # This relies on instances set up by "resolve recursively builds instances..."
    bang = Schema_Builder.instance('TestJunk::Foo::Bif::Bang')
    bang.expanded_schema_attributes.should == {
      "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema", 
      "elementFormDefault"=>"qualified", 
      :xmlns=>"urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang", 
      :targetNamespace=>"urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang", 
      "xmlns:TestJunk_Foo_Bif_Bang"=>"urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang", 
      "xmlns:TestJunk_Bazinga"=>"urn:prometheuscomputing.com/TestJunk_Bazinga", 
      "xmlns:TestJunk_FiddleFaddle"=>"urn:prometheuscomputing.com/TestJunk_FiddleFaddle",
      "xmlns:TestJunk_Toasted" => "urn:prometheuscomputing.com/TestJunk_Toasted",
      "xmlns:Piggy" => "urn:prometheuscomputing.com/Piggy"
      }
  end
  
  it "generates and writes all schemas required for the specified roots" do
    # Specify json/ruby getter mappings (for the use case we want to generate a schema for)
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION_2.clone
    # Remove any existing Schema_Builder instances
    Schema_Builder.clear
    # Specify which Strings are actually Enumerations (must be done before instance creation)
    Schema_Builder.type_override = {
      [TestJunk::Bazinga::SoftKitty, :eye_color] => Unknown::EyeColor,
      [TestJunk::Foo::Bif::Bang::Crackle, :material] => Piggy::HousingMaterial
    }
    Schema_Builder.root_classes = [TestJunk::Foo::Bif::Bang::Crackle]

    bldr = Schema_Builder.find('TestJunk::Foo::Bif::Bang')
    bldra = bldr.root_classes
    # puts ">>>>>>>>>>>>>>>>> #{bldr.root_classes.inspect} <<<<<<<"
    
    # Resolves references and then generates a schema corresponding to each Schema_Builder instance. If a directory is provided, the generated schemas are written
    Schema_Builder.make_schemas ACTUAL_DIR
    
    
    bldr2 = Schema_Builder.find('TestJunk::Foo::Bif::Bang')
    bldr2.should equal(bldr)
    # puts "================ #{bldr.root_classes.inspect} <<<<<<<"
    bldra.should equal(bldr.root_classes)
    
    package_paths = Schema_Builder.all_instances.collect {|inst| inst.package_path }.sort
    # Note that "TestJunk::Foo", "TestJunk::Foo::Bif", "TestJunk::Foo::Bif::Boom are not included.
    # It's because they don't contain anything referenced by the root_classes.
    package_paths.should == ["Piggy", "TestJunk::Bazinga", "TestJunk::FiddleFaddle", "TestJunk::Foo::Bif::Bang", "TestJunk::Toasted"]
    files = ['Piggy.xsd', 'TestJunk/Bazinga.xsd', 'TestJunk/FiddleFaddle.xsd', 'TestJunk/Toasted.xsd', 'TestJunk/Foo/Bif/Bang.xsd']
    files.each {|filepath| compare(filepath) }
  end

  ################################

  it "finds Reference_Info instances for attibutes and associations" do
    JSON_Graph_State.json_getter_overrides_by_class = MARSHALING_SPECIFICATION_3.clone
    Reference_Info.clear
    # Remove any existing Schema_Builder instances
    Schema_Builder.clear
    # Specify which Strings are actually Enumerations (must be done before instance creation)
    Schema_Builder.type_override = {
      [TestJunk::Bazinga::SoftKitty, :eye_color] => Unknown::EyeColor,
      [TestJunk::Foo::Bif::Bang::Crackle, :material] => Piggy::HousingMaterial
    }
    Schema_Builder.root_classes = [TestJunk::Foo::Bif::Bang::Crackle]
    Schema_Builder.make_schemas
    
    eye_color = Reference_Info.lookup(TestJunk::Bazinga::SoftKitty, "urn:prometheuscomputing.com/TestJunk_Bazinga", :eye_color)
    eye_color.should_not be_nil
    eye_color.namespace_uri.should == "urn:prometheuscomputing.com/TestJunk_Bazinga"
    eye_color.inherited.should be false
    eye_color.multiplicity.should == Range.new(0, 1)
    
    material = Reference_Info.lookup(TestJunk::Foo::Bif::Bang::Crackle, "urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang", :material)
    material.should_not be_nil
    material.namespace_uri.should == "urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang"
    material.inherited.should be false
    material.multiplicity.should == Range.new(0, 1)
    
    
    rubbish = Reference_Info.lookup(TestJunk::Foo::Bif::Bang::Crackle, "urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang", :rubbish)
    rubbish.should_not be_nil
    rubbish.namespace_uri.should == "urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang"
    rubbish.inherited.should be false
    rubbish.multiplicity.should == Range.new(0, 1)
    
    
    junque = Reference_Info.lookup(TestJunk::Foo::Bif::Bang::Crackle, "urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang", :junque)
    junque.should_not be_nil
    junque.namespace_uri.should == "urn:prometheuscomputing.com/TestJunk_Foo_Bif_Bang"
    junque.inherited.should be false
    junque.multiplicity.should == Range.new(0, 1)

  end
  
end


