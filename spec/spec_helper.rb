require 'fileutils'


require 'Foundation/load_path_management'
EXPECTED_DIR = relative('../data/test_expected')
ACTUAL_DIR = relative('../tmp')
OPTIONS = {:max_nesting => 1000}

# Use local version of json_graph
$:.unshift relative('../lib')
require 'json_graph'
require 'json_graph/xml_support'
require 'json_graph/xml_schema_builder'
require 'model_metadata'
require 'pp'
require_relative 'family_example'
require_relative 'test_junk_example'

class String
  def fileName_to_actual_path; File.join(ACTUAL_DIR, self); end
  def fileName_to_expected_path; File.join(EXPECTED_DIR, self); end
end

class Hash
  # Replaces nil values with keys
  def to_mapping
    answer = clone
    each {|key, value| answer[key]= key unless value }
    answer
  end
end

# To help with failed Time comparisons, override Time's inspect method to show milliseconds
class Time
  def inspect
    self.strftime "%Y-%m-%d %H:%M:%S:%9N %z"
  end
end

# These methods are used during testing, in place of SSA method.
# These methods should not be used for production.
class Module
  def interface?; @is_interface||=false; end
  def is_interface=(boolean); @is_interface||=false; end
end

# Compares actual and expected file content.
# If actual_content is provided:
#   Writes actual_content to filename in directory ACTUAL_DIR.
#   Then compares actual_content to the the content of the file with the same name in EXPECTED_DIR
# If actual_content is not provided:
#   Compares content of file in EXPECTED_DIR with file in ACTUAL_DIR
def compare(filename, actual_content=nil)
  actual_path = filename.fileName_to_actual_path
  expected_path = filename.fileName_to_expected_path
  raise "Expected file did not exist: #{expected_path.inspect}" unless File.exist?(expected_path)
  expected_content = File.read(expected_path)
  if actual_content
    actual_dir_path = File.dirname(actual_path)
    FileUtils::Verbose.mkdir_p(actual_dir_path) unless File.exist?(actual_dir_path)
    File.open(actual_path, 'w') {|f| f << actual_content }
  else
    raise "Actual file did not exist: #{actual_path.inspect}" unless File.exist?(actual_path)
    actual_content = File.read( actual_path )
  end
  actual_content.should == expected_content
end

