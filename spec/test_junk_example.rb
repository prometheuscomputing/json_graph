module Unknown; end
module Piggy; end
Enumeration.create :'Piggy::HousingMaterial', 'Straw', 'Sticks', 'Bricks', 'Titanium'
Enumeration.create :Marsupials, 'Kangaroo', 'Koala', 'Duck-billed platypus'
Enumeration.create :'Unknown::EyeColor', 'Fuligin', 'Octarine', 'Smaudre', 'Rawn', 'Pallow'

module TestJunk
  module Toasted
    class Cheese
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; []; end
      def self.attributes; {}; end
      def self.associations; {}; end
    end
  end
  module FiddleFaddle
    class SomeInterface 
      is_interface=true  # SSA uses classes to represent interfaces
      def self.interface?; true; end
      def self.implementors; [TestJunk::Bazinga::SoftKitty, TestJunk::Foo::Bif::Bang::Snap]; end
    end
  end
  module Bazinga
    class SoftKitty
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; [TestJunk::FiddleFaddle::SomeInterface]; end 
      def self.attributes; { :eye_color => {:class => 'String'}}; end
      def self.associations; {}; end
    end
  end
  module Foo
    class Bar
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; []; end
      def self.attributes; {}; end
      def self.associations; {}; end
    end
    class Baz
      include JSON_Graph::Serializeable
      def self.parents; []; end
      def self.interfaces; []; end
      def self.attributes; {}; end
      def self.associations; {}; end
    end
    module Bif
      module Bang
        class Snap
          include JSON_Graph::Serializeable
          def self.parents; []; end
          def self.interfaces; [TestJunk::FiddleFaddle::SomeInterface]; end
          def self.attributes; {}; end
          def self.associations; {}; end
        end
        class Crackle < TestJunk::Toasted::Cheese
          include JSON_Graph::Serializeable
          def self.parents; [TestJunk::Toasted::Cheese]; end
          def self.interfaces; []; end
          def self.attributes; { :material => {:class => 'String'} }; end
          def self.associations; { :rubbish => {:class => 'TestJunk::Bazinga::SoftKitty'}, :junque => {:class => 'TestJunk::FiddleFaddle::SomeInterface'} }; end
        end
      end
      module Boom
        class Pop
          include JSON_Graph::Serializeable
          def self.parents; []; end
          def self.interfaces; []; end
          def self.attributes; {}; end
          def self.associations; {}; end
        end
      end
    end
  end
end

# This omits SoftKitty.eye_color
MARSHALING_SPECIFICATION_2 = {
  TestJunk::Foo::Bif::Bang::Crackle => 
      {
        :rubbish => nil,
        :junque => nil,
        :material => nil
      }
}

MARSHALING_SPECIFICATION_3 = {
  TestJunk::Bazinga::SoftKitty => { :eye_color => nil},
  TestJunk::Foo::Bif::Bang::Crackle => 
      {
        :rubbish => nil,
        :junque => nil,
        :material => nil
      }
}